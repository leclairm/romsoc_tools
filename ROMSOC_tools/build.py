import os
import subprocess


class build_script(object):
    """ROMSOC compiling script object"""

    _all_targets = ['oasis', 'roms', 'cosmo']
    _modules = ['intel/15.0.0', 'netcdf/4.3.1', 'open_mpi/1.6.5']
    _romsoc_env_vars = ['ROMSOC_OASIS_HOME', 'ROMSOC_OASIS_INSTALL',
                        'ROMSOC_ECCODES_INSTALL', 'ROMSOC_LIBGRIB1',
                        'ROMSOC_COSMO_HOME', 'ROMSOC_ROMS_HOME']
    
    
    def __init__(self, targets, run=False, coupled=True):

        self.targets = targets
        self.run = run
        self.coupled = coupled
        self.content = """#!/usr/bin/bash

# =============================================================== #
# Romsoc compiling script automatically generated by ROMSOC_tools #
# =============================================================== #
"""
        # Prepare script
        self._set_env()
        if 'oasis' in self._targets and self.coupled:
            self._add_oasis()
        if 'cosmo' in self._targets:
            self._add_cosmo()
        if 'roms' in self._targets:
            self._add_roms()

        # Write script
        with open(self._file_name, mode='w') as script:
            script.write(self.content)
        os.chmod(self._file_name, 0o755)

        # Run script
        if self.run:
            subprocess.run(['./' + self._file_name])

    @property
    def targets(self):
        return self._targets
    
    @targets.setter
    def targets(self, trgts):
        trgts = [trgt.lower() for trgt in trgts]
        if 'all' in trgts:
            self._targets = self._all_targets
        else:
            msg = "{:s} is not a valid target. Has to be (non case sensitive) within {}"
            for trgt in trgts:
                if trgt not in self._all_targets:
                    raise ValueError(msg.format(trgt, self._all_targets))
            self._targets = trgts
        self._file_name = 'romsoc_build_' + '_'.join(self._targets)


    def _set_env(self):
        """Check ROMSOC environment variables and load modules"""

        # Check missing environment variables
        missing_vars = [var for var in self._romsoc_env_vars if var not in os.environ]
        if missing_vars:
            msg = "Following environment variables need to be set: {}"
            raise EnvironmentError(msg.format(missing_vars))

        # Load modules
        self.content += """
# Set compiling environment
# -------------------------
"""
        for module in self._modules:
            self.content += 'module load {}\n'.format(module)


    def _add_oasis(self):
        """Add OASIS compiling instructions"""

        self.content += """
# Build OASIS3 library
# --------------------
echo
echo "--------------- Building OASIS ---------------"
echo
export OASIS_MACH=euler.intel
cd ${ROMSOC_OASIS_HOME}/util/make_dir
make -f TopMakefileOasis3
"""

        
    def _add_cosmo(self):
        """Add COSMO compiling instructions"""

        self.content += """
# Build COSMO
# -----------
echo
echo "--------------- Building COSMO ---------------"
echo
export LD_LIBRARY_PATH=${{ROMSOC_ECCODES_INSTALL}}/lib:$LD_LIBRARY_PATH
export COSMO_MACH=euler_intel
cd ${{ROMSOC_COSMO_HOME}}
make paropt{}
""".format('cpl' if self.coupled else '')


    def _add_roms(self):
        """Add ROMS compiling instructions"""
        
        self.content += """
# Build ROMS
# -----------
echo
echo "--------------- Building ROMS ---------------"
echo
export LD_LIBRARY_PATH=${ROMSOC_ECCODES_INSTALL}/lib:$LD_LIBRARY_PATH
export COSMO_MACH=euler_intel
cd ${ROMSOC_ROMS_HOME}/src
branch=$(git rev-parse --abbrev-ref HEAD)"""
        if self.coupled:
            self.content += """
if [[ $branch != ROMSOC ]]; then
  echo "ERROR: need to be on ROMSOC branch to build coupled version"; exit 1
fi
make roms_cpl
"""
        else:
            self.content += """
if [[ $branch == ROMSOC ]]; then
  echo "ERROR: cannot build non coupled version on ROMSOC branch"; exit 1
fi
make roms
"""

import numpy as np


class displaced_poles_mapping():
    """
    This class implements a displaced poles mappping.

    Paramters
    ---------
    NP : array_like(shape=(2,))
        (lon, lat) coordinates of the displaced north pole in degrees.
    SP : array_like(shape=(2,)), optional
        (lon, lat) coordinates of the displaced south pole in degrees.
        If not given, the antipode of NP is used.

    Notes
    -----
    Following [1] The mapping is obtained by the composition of the
    south pole stereographic projection into the complex plane, a
    conformal mapping in the complex plane and the inverse projection
    from the complex plane back on the sphere.

    References
    ----------
    [1] Bentsen, M., G. Evensen, H. Drange, and A.D. Jenkins, 1999:
    Coordinate Transformation on a Sphere Using Conformal
    Mapping. Mon. Wea. Rev., 127, 2733–2740,
    https://doi.org/10.1175/1520-0493(1999)127<2733:CTOASU>2.0.CO;2

    """

    def __init__(self, NP, SP=None):

        # Init poles
        self.NP = NP
        self.SP = SP

        # Init mapping subfunctions
        self.spheric_to_3d = spheric_to_cartesian()
        self.stereo_proj = complex_south_stereo_proj()
        self.conf_map = complex_conformal_mapping(self.NP, self.SP)

    @property
    def NP(self):
        return self._NP
    
    @NP.setter
    def NP(self, lonlat):
        self._NP = np.asarray(lonlat)

    @property
    def SP(self):
        return self._SP
    
    @SP.setter
    def SP(self, lonlat):
        if lonlat is None:
            self._SP = np.array(((self._NP[0])+180, -self._NP[1]))
        else:
            self._SP = np.asarray(lonlat)


    def direct_map(self, P):
        """
        Direct mapping : from geographical to displaced pole.

        Parameters
        ----------
        P : ndarray(shape=s+(2,))
            (lon, lat) coordinates in degrees of points to be mapped.

        Returns
        -------
        Q : ndarray(shape=s+(2,))
            (lon, lat) coordinates in degrees of mapped points."""

        X = self.spheric_to_3d.direct_map(P)
        w = self.stereo_proj.direct_map(X)
        z = self.conf_map.inverse_map(w)
        Y = self.stereo_proj.inverse_map(z)
        Q = self.spheric_to_3d.inverse_map(Y)

        return Q


    def inverse_map(self, Q):
        """
        Inverse mapping : from displaced pole to geographical.

        Parameters
        ----------
        Q : ndarray(shape=s+(2,))
            (lon, lat) coordinates in degrees of points to be mapped.

        Returns
        -------
        P : ndarray(shape=s+(2,))
            (lon, lat) coordinates in degrees of mapped points."""

        Y = self.spheric_to_3d.direct_map(Q)
        z = self.stereo_proj.direct_map(Y)
        w = self.conf_map.direct_map(z)
        X = self.stereo_proj.inverse_map(w)
        P = self.spheric_to_3d.inverse_map(X)

        return P


    def directions(self, P, reorder=False):
        """
        3D local directions of the displaced poles mapping.

        The local 3D directions are computed along varrying displaced
        lon and lat, i.e. along displaced parallels and merdians,
        assuming both are growing.

        Parameters
        ----------
        P : ndarray(shape=s+(2,))
            (lon, lat) coordinates in degrees of points where directions
            are computed.
        reorder : bool, default False
            return an ndarray reordered in a more "natural" way

        Returns
        -------
        grid_dir : ndarray(shape=s_grd, dtype='float64')

            If reorder is False, s_grd=s+(3,2), else s_grd=(2,3)+s.
            Local grid 3D direction matrix. It is essentially a
            jacobian matrix normalized along the space dimension so
            that directions are represented by unit vectors.
            Coordinates are expressed in the geographical (x,y,z)
            frame of reference and correspond to the before last
            (second if reorder) dimension. The first index of the last
            dimension (first if reorder) corresponds to the direction
            along the displaced parallel and the second one along the
            displaced meridian.

        """

        X = self.spheric_to_3d.direct_map(P)
        z = self.stereo_proj.direct_map(X)
        D_proj = self.stereo_proj.inverse_jacobian(z)

        w = self.conf_map.direct_map(z)
        D_conf = self.conf_map.inverse_jacobian(w)
        D_conf[...,1] = - D_conf[...,1]   # assume displaced lat is growing

        # Compute Jacobian matrix
        D_map = D_proj @ D_conf
        # Normalize
        D_map = D_map / np.linalg.norm(D_map, ord=2, axis=-2, keepdims=True)
        # Reorder
        if reorder:
            D_map = np.moveaxis(D_map, (-1,-2), (0,1))
        # Return
        return D_map


class spheric_to_cartesian():
    """
    Class for the conversion from (lon, lat) coordinates to cartesian and back.

    """

    def direct_map(self, P):
        """
        Compute 3D cartesian coordinates on the unit sphere from (lon, lat).

        Parameters
        ----------
        P : ndarray(shape=s+(2,), type='float64'
            (lon, lat) coordinates in degrees.

        Returns
        -------
        X : ndarray(shape=s+(3,))
            Cartesian coordinates.

        """

        P = np.deg2rad(P)
        lbda, phi = P[...,1], P[...,0]
        c_lbda, s_lbda = np.cos(lbda), np.sin(lbda)
        c_phi, s_phi = np.cos(phi), np.sin(phi)

        return np.stack((c_lbda*c_phi, c_lbda*s_phi, s_lbda), axis=-1)


    def inverse_map(self, X):
        """
        Compute geographical (lon, lat) from 3D cartesian coordinates on the sphere.

        Paramters
        ---------
        X : ndarray(shape=s+(3,))
            Cartesian coordinates.

        Returns
        -------
        P : ndarray(shape=s+(2,))
            (lon, lat) coordinates in degrees.

        """

        return np.rad2deg(np.stack((np.arctan2(X[...,1], X[...,0]),
                                    np.arcsin(X[...,2])), axis=-1))


class complex_south_stereo_proj():
    """
    Class for the south pole stereographic projection onto the complex space and back.

    """

    def direct_map(self, X):
        """
        South pole stereographic projection onto the complex space.

        Parameters
        ----------
        X : ndarray(shape=s+(3,))
            Cartesian coordinates on the unit sphere.

        Returns
        -------
        z : ndarray(shape=s, dtype='complex128')
            Positions in the complex space of stereographic projections.

        """

        return 1.0/(1.0+X[...,2]) * (X[...,0] + 1j*X[...,1])


    def inverse_map(self, z):
        """
        Inverse south pole stereographic projection onto the unit sphere.

        Parameters
        ----------
        z : ndarray(shape=s, dtype='complex128')
            Positions in the complex space of stereographic projections.

        Returns
        -------
        X : ndarray(shape=s+(3,))
            Cartesian coordinates on the unit sphere.

        """

        x, y = np.real(z), np.imag(z)
        r2 = x**2 + y**2
        D = 1.0 / (1.0+r2)

        X = np.empty(z.shape+(3,), dtype='float64')
        X[...,0] = D * 2.0 * x
        X[...,1] = D * 2.0 * y
        X[...,2] = D * (1.0-r2)

        return X


    def inverse_jacobian(self, z):
        """
        Jacobian matrix of inverse south pole stereographic projection.

        Parameters
        ----------
        z : ndarray(shape=s, dtype='complex128')
            Complex positions where the jacobian is evaluated.

        Returns
        -------
        jcbn : ndarray(shape=s+(3,2), dtype='float64')
            Jacobian matrix of (alpha, beta) -> (x1,x2,x3) where
            z=alpha+i*beta, X=(x1,x2,x3) and z->X is the inverse
            complex south pole stereographic projection.

        """

        x, y = np.real(z), np.imag(z)
        x2, y2 = x**2, y**2
        D = 2.0 / (1.0+x2+y2)**2

        jcbn = np.empty(z.shape+(3,2), dtype='float64')
        jcbn[...,0,0] = D * (1.0-x2+y2)
        jcbn[...,1,0] = -2.0 * D * x * y
        jcbn[...,2,0] = -2.0 * D * x
        jcbn[...,0,1] = -2.0 * D * x * y
        jcbn[...,1,1] = D * (1.0+x2-y2)
        jcbn[...,2,1] = -2.0 * D * y

        return jcbn


class complex_conformal_mapping():
    """
    This class implements a conformal mapping in the complex plane.

    Parameters
    ----------
    NP : array_like(shape=(2,))
        (lon, lat) coordinates of the displaced north pole in degrees.
    SP : array_like(shape=(2,))
        (lon, lat) coordinates of the displaced south pole in degrees.

    Notes
    -----
    This implementation follows [1].

    References
    ----------
    [1] Bentsen, M., G. Evensen, H. Drange, and A.D. Jenkins, 1999:
    Coordinate Transformation on a Sphere Using Conformal
    Mapping. Mon. Wea. Rev., 127, 2733–2740,
    https://doi.org/10.1175/1520-0493(1999)127<2733:CTOASU>2.0.CO;2

    """

    def __init__(self, NP, SP):
        
        self.NP = NP
        self.SP = SP
        self.spheric_to_3d = spheric_to_cartesian()
        self.stereo_proj = complex_south_stereo_proj()
        self.set_conformal_map_params()


    def set_conformal_map_params(self):
        """Set conformal map complex parameters.

        Set the complex parameters of the complex conformal mapping
        function, i.e. the complex south pole stereographic
        projections of the displaced poles and their midpoint.

        Notes
        -----
        Only having the the locations of the displaced poles isn't
        enough to uniquely define the confrmal mapping. So it's also
        assumed that the midpoint between the displaced poles maps to
        the equator at longitude 0, so that the image of the grand
        circle symetrically separating the displaced poles is the
        equator.

        """

        # 3D cartesian coordinates
        N3 = self.spheric_to_3d.direct_map(self.NP)
        S3 = self.spheric_to_3d.direct_map(self.SP)
        C3 = N3 + S3
        n_C3 = np.linalg.norm(C3)
        eps = 1.e-5
        if n_C3 <= eps:
            # delta = self.NP[1] < 0
            # CP = np.array((self.NP[0]-delta*180.0, np.abs(self.NP[1])-90.0))
            delta = self.NP[1] > 0
            CP = np.array((self.NP[0]+delta*180.0, 90.0-np.abs(self.NP[1])))
            C3 = self.spheric_to_3d.direct_map(CP)
        else:
            C3 /= n_C3

        # South stereographic complex projections
        self.n = self.stereo_proj.direct_map(N3)
        self.s = self.stereo_proj.direct_map(S3)
        self.c = self.stereo_proj.direct_map(C3)


    def direct_map(self, z):
        """
        Conformal mapping in the complex plane.

        Parameters
        ----------
        z : ndarray(shape=s, dtype='complex128')
            Complex positions where the conformal mapping is evaluated.

        Returns
        -------
        w : ndarray(shape=s, dtype='complex128')
            Mapped complex positions.

        """

        return (z-self.n)*(self.c-self.s) / ((z-self.s)*(self.c-self.n))



    def inverse_map(self, w):
        """
        Inverse conformal mapping in the complex plane.

        Parameters
        ----------
        w : ndarray(shape=s, dtype='complex128')
            Complex positions where the inverse conformal mapping is
            evaluated.

        Returns
        -------
        z : ndarray(shape=s, dtype='complex128')
            Inverse mapped complex positions.

        """

        return (self.s*(self.n-self.c)*w + self.n*(self.c-self.s)) \
            / ((self.n-self.c)*w + (self.c-self.s))


    def inverse_jacobian(self, w):
        """
        Jacobian matrix of inverse conformal mapping.

        Parameters
        ----------
        w : ndarray(shape=s, dtype='complex128')
            Complex positions where the jacobian is evaluated.

        Returns
        -------
        jcbn : ndarray(shape=s+(2,2), dtype='complex128')
            Jacobian matrix of the the function (theta, rho) -> (alpha,
            beta) where w=rho*exp(i*theta), z=alpha+i*beta, and w->z is
            the inverse conformal mapping function.

        """

        # Mapping complex derivative
        dz_dw = (self.n-self.c)*(self.s-self.n)*(self.c-self.s) \
            / ((self.n-self.c)*w+(self.c-self.s))**2

        # Mapping complex derivatives in the (theta, rho) directions
        dz_dtheta = dz_dw * 1j * w
        dz_drho = dz_dw * w / np.absolute(w)

        # Jacobian matrix
        jcbn = np.empty(w.shape+(2,2), dtype='float64')
        jcbn[...,0,0] = np.real(dz_dtheta)
        jcbn[...,1,0] = np.imag(dz_dtheta)
        jcbn[...,0,1] = np.real(dz_drho)
        jcbn[...,1,1] = np.imag(dz_drho)

        return jcbn

from .displaced_poles_mapping import displaced_poles_mapping as dp_map
from argparse import ArgumentParser, RawTextHelpFormatter
import xarray as xr
import numpy as np


def get_ROMSOC_ds(ROMS_grd_file, COSMO_grd_file, transition_width):
    """
    Generate the ROMSOC dataset object.

    Parameters
    ----------
    ROMS_grd_file : str
        Path to the ROMS netcdf grid file
    COSMO_grd_file : str
        Path to the COSMO netcdf grid file
    transition_width : int
        Width of the transition layer from surface forcing to coupling
        in number of COSMO grid points

    Returns
    -------
    ROMSOC_ds : xarray.Dataset object
        This is the in-memory representation of the ROMSOC netcdf
        file. The dataset contains:
        - velocity directions as unit vectors expressed in the
          cartesian coordinate system.
        - weighting coefficient needed for the transition from surface
          forcing to coupling with COSMO.

    Notes
    -----
    A future version should rather take the COSMO namelist holding the
    grid information as input rather than a netcdf grid file because
    there is actually no such thing for COSMO. In the present state,
    the COSMO grid info is read in an output file so that an uncoupled
    COSMO run - even a dummy one - is necessary in the first place.

    """
    
    # Read in data and create ROMSOC dataset
    # ======================================
    romsoc_ds = xr.Dataset()
    with xr.open_dataset(ROMS_grd_file) as roms_grd, xr.open_dataset(COSMO_grd_file) as cos_grd:
        romsoc_ds.coords['lon_rho'] = roms_grd.coords['lon_rho']
        romsoc_ds.coords['lat_rho'] = roms_grd.coords['lat_rho']
        romsoc_ds.coords['lon_u'] = roms_grd.coords['lon_u']
        romsoc_ds.coords['lat_u'] = roms_grd.coords['lat_u']
        romsoc_ds.coords['lon_v'] = roms_grd.coords['lon_v']
        romsoc_ds.coords['lat_v'] = roms_grd.coords['lat_v']

        romsoc_ds.coords['lon'] = cos_grd.coords['lon']
        romsoc_ds.coords['lat'] = cos_grd.coords['lat']
        romsoc_ds.coords['slonu'] = cos_grd.coords['slonu']
        romsoc_ds.coords['slatu'] = cos_grd.coords['slatu']
        romsoc_ds.coords['slonv'] = cos_grd.coords['slonv']
        romsoc_ds.coords['slatv'] = cos_grd.coords['slatv']

        NP = roms_grd.attrs['putpoles_northpole']
        if 'putpoles_southpole' in roms_grd.attrs:
            SP = roms_grd.attrs['putpoles_southpole']
        else:
            SP = None
        roms_map = dp_map(NP, SP=SP)
        NP = (cos_grd['rotated_pole'].attrs['grid_north_pole_longitude'],
              cos_grd['rotated_pole'].attrs['grid_north_pole_latitude'])
        cos_map = dp_map(NP)

    drop_vars = [v for v in ('height_2m', 'height_10m') if v in romsoc_ds]
    if drop_vars:
        romsoc_ds = romsoc_ds.drop(drop_vars)

    # Add velocity directions
    # =======================
    # At ROMS u-points
    # ----------------
    P = np.stack((romsoc_ds['lon_u'], romsoc_ds['lat_u']), axis=-1)
    roms_dir = roms_map.directions(P, reorder=True)
    cos_dir = cos_map.directions(P, reorder=True)
    romsoc_ds['roms_u_dir'] = xr.DataArray(roms_dir[0,...],
                                           dims=('space', 'eta_u', 'xi_u'),
                                           name='roms_u_dir',
                                           attrs={'long_name': 'ROMS velocity u-component direction',
                                                  'units': '1'})
    romsoc_ds['cos_u_dir_roms_u'] = xr.DataArray(cos_dir[0,...],
                                                 dims=('space', 'eta_u', 'xi_u'),
                                                 name='cos_u_dir_roms_u',
                                                 attrs={'long_name': 'COSMO velocity u-component direction at ROMS u-points',
                                                        'units': '1'})
    romsoc_ds['cos_v_dir_roms_u'] = xr.DataArray(cos_dir[1,...],
                                                 dims=('space', 'eta_u', 'xi_u'),
                                                 name='cos_v_dir_roms_u',
                                                 attrs={'long_name': 'COSMO velocity v-component direction at ROMS u-points',
                                                        'units': '1'})

    # At ROMS v-points
    # ----------------
    P = np.stack((romsoc_ds['lon_v'], romsoc_ds['lat_v']), axis=-1)
    roms_dir = roms_map.directions(P, reorder=True)
    cos_dir = cos_map.directions(P, reorder=True)
    romsoc_ds['roms_v_dir'] = xr.DataArray(roms_dir[1,...],
                                           dims=('space', 'eta_v', 'xi_v'),
                                           name='roms_v_dir',
                                           attrs={'long_name': 'ROMS velocity v-component direction',
                                                  'units': '1'})
    romsoc_ds['cos_u_dir_roms_v'] = xr.DataArray(cos_dir[0,...],
                                                 dims=('space', 'eta_v', 'xi_v'),
                                                 name='cos_u_dir_roms_v',
                                                 attrs={'long_name': 'COSMO velocity u-component direction at ROMS v-points',
                                                        'units': '1'})
    romsoc_ds['cos_v_dir_roms_v'] = xr.DataArray(cos_dir[1,...],
                                                 dims=('space', 'eta_v', 'xi_v'),
                                                 name='cos_v_dir_roms_v',
                                                 attrs={'long_name': 'COSMO velocity v-component direction at ROMS v-points',
                                                        'units': '1'})

    # At COSMO u-points
    # -----------------
    P = np.stack((romsoc_ds['slonu'], romsoc_ds['slatu']),axis=-1)
    cos_dir = cos_map.directions(P, reorder=True)
    roms_dir = roms_map.directions(P, reorder=True)
    romsoc_ds['cos_u_dir'] = xr.DataArray(cos_dir[0,...],
                                          dims=('space', 'rlat', 'srlon'),
                                          name='cos_u_dir',
                                          attrs={'long_name': 'COSMO velocity u-component direction',
                                                 'units': '1'})
    romsoc_ds['roms_u_dir_cos_u'] = xr.DataArray(roms_dir[0,...],
                                                 dims=('space', 'rlat', 'srlon'),
                                                 name='roms_u_dir_cos_u',
                                                 attrs={'long_name': 'ROMS velocity u-component direction at COSMO u-points',
                                                        'units': '1'})
    romsoc_ds['roms_v_dir_cos_u'] = xr.DataArray(roms_dir[1,...],
                                                 dims=('space', 'rlat', 'srlon'),
                                                 name='roms_v_dir_cos_u',
                                                 attrs={'long_name': 'ROMS velocity v-component direction at COSMO u-points',
                                                        'units': '1'})

    # At COSMO v-points
    # -----------------
    P = np.stack((romsoc_ds['slonv'], romsoc_ds['slatv']),axis=-1)
    cos_dir = cos_map.directions(P, reorder=True)
    roms_dir = roms_map.directions(P, reorder=True)
    romsoc_ds['cos_v_dir'] = xr.DataArray(cos_dir[1,...],
                                          dims=('space', 'srlat', 'rlon'),
                                          name='cos_v_dir',
                                          attrs={'long_name': 'COSMO velocity v-component direction',
                                                 'units': '1'})
    romsoc_ds['roms_u_dir_cos_v'] = xr.DataArray(roms_dir[0,...],
                                                 dims=('space', 'srlat', 'rlon'),
                                                 name='roms_u_dir_cos_v',
                                                 attrs={'long_name': 'ROMS velocity u-component direction at COSMO v-points',
                                                        'units': '1'})
    romsoc_ds['roms_v_dir_cos_v'] = xr.DataArray(roms_dir[0,...],
                                                 dims=('space', 'srlat', 'rlon'),
                                                 name='roms_v_dir_cos_v',
                                                 attrs={'long_name': 'ROMS velocity v-component direction at COSMO v-points',
                                                        'units': '1'})
    # Add weihgting coefficient for the ROMS grid
    # ===========================================
    # - ML - This part shoud move to a module or at least a function of its own.
    # Function computing the coefficient
    # ----------------------------------
    std_a = lambda x : (x + 180.0) % 360.0 - 180.0
    rlon, rlat = romsoc_ds['rlon'].values, romsoc_ds['rlat'].values
    rlon_0, rlat_0 = rlon[int(np.floor(0.5*len(rlon)))], rlat[int(np.floor(0.5*len(rlat)))]
    rlon, rlat = std_a(rlon-rlon_0), std_a(rlat-rlat_0)
    
    rlon_min, rlon_max = rlon[0], rlon[-1]
    rlat_min, rlat_max = rlat[0], rlat[-1]
    d_rlon = rlon[transition_width-1] - rlon_min
    d_rlat = rlat[transition_width-1] - rlat_min
    
    
    def profile(x, x0, xmin, xmax, dx):

        x = std_a(x-x0)
        p0 = np.clip((x-xmin)/dx, 0.0, 1.0)
        p1 = np.clip((xmax-x)/dx, 0.0, 1.0)
        
        return p0*p1
    
    
    def alpha_couple(lon, lat):

        P = np.stack((lon, lat),axis=-1)
        Q = cos_map.inverse_map(P)
        rlon, rlat = Q[...,0], Q[...,1]
        
        return profile(rlon, rlon_0, rlon_min, rlon_max, d_rlon) \
            * profile(rlat, rlat_0, rlat_min, rlat_max, d_rlat)

    # At rho-points
    # -------------
    romsoc_ds['alpha_rho'] = xr.DataArray(alpha_couple(romsoc_ds.lon_rho.values, romsoc_ds.lat_rho.values),
                                          dims=('eta_rho', 'xi_rho'),
                                          name='alpha_rho',
                                          attrs={'long_name': 'coupling coefficient at ROMS rho-points',
                                                 'units': '1'})
    # At u-points
    # -----------
    romsoc_ds['alpha_u'] = xr.DataArray(alpha_couple(romsoc_ds.lon_u.values, romsoc_ds.lat_u.values),
                                        dims=('eta_u', 'xi_u'),
                                        name='alpha_u',
                                        attrs={'long_name': 'coupling coefficient at ROMS u-points',
                                               'units': '1'})
    # At v-points
    # -----------
    romsoc_ds['alpha_v'] = xr.DataArray(alpha_couple(romsoc_ds.lon_v.values, romsoc_ds.lat_v.values),
                                        dims=('eta_v', 'xi_v'),
                                        name='alpha_v',
                                        attrs={'long_name': 'coupling coefficient at ROMS v-points',
                                               'units': '1'})
    
    # Return dataset
    # ==============
    return romsoc_ds

from argparse import ArgumentParser, RawTextHelpFormatter
from .gen_ROMSOC import get_ROMSOC_ds
from .gen_grid import get_dp_grid_ds
from .add_corners import add_corners
from .run_tools.ROMSOC_Params_default import params as default_run_params
from .run_tools.ROMSOC_Run import ROMSOC_Run
from .run_tools.gen_OASIS_rst import gen_oas_rst
from .build import build_script
from pathlib import Path


def main():

    # Setup parsers
    # =============
    dsc = """ROMSOC tools is a suite of tools developped around the coupled
ROMSOC model (ROMS coupled with COSMO)"""
    parser = ArgumentParser(description=dsc)
    subparsers = parser.add_subparsers(title='commands',
                                       dest='command')

    # Auxialiary ROMSOC file command parser
    # -------------------------------------
    dsc = """Create ROMSOC auxialiary file.

The output file contains:
- Velocity components directions of ROMS and COSMO on each grid.
- Coupling coefficient on the ROMS grid indicating if the point is
  in a forced, coupled or mixed mode. The coefficient goes from 0
  outside of the COSMO domain to 1 inside after some
  transition.

All the computations are done analytically based on a displaced
pole mapping since both grids are analytical.

A future version should rather take the COSMO namelist holding the
grid information as input rather than a netcdf grid file because
there is actually no such thing for COSMO. In the present state,
the COSMO grid info is read in an output file so that an uncoupled
COSMO run - even a dummy one - is necessary in the first place."""
    aux_parser = subparsers.add_parser('aux', help="Create ROMSOC auxiliary file.",
                                       description=dsc, formatter_class=RawTextHelpFormatter)
        
    aux_parser.add_argument('--roms', metavar='grd', required=True,
                            help="Path to ROMS grid file, required.")
    aux_parser.add_argument('--cosmo', metavar='grd', required=True,
                            help="Path to file containing the COSMO grid info, required.")
    aux_parser.add_argument('--delta', metavar='k', required=True, type=int,
                            help="width of the transition layer from forced to coupled mode\n"
                            "given in number of COSMO grid points, required.")
    aux_parser.add_argument('-o', '--out', required=True, help="Path of the output file, required.")

    # Displaced pole ROMS grid command parser
    # ---------------------------------------
    # Generate lon, lat floats from input strings
    lonlat = lambda s : list(map(float, s.split(',')))
    dsc = """Create displaced poles grids with ROMS conventions.

All computations are done analytically based on a displaced pole mapping
function following (Bentsen et al. MWRe 1999)."""

    dp_grid_parser = subparsers.add_parser('dp_grid', help="create ROMS displaced poles grid.",
                                           description=dsc, formatter_class=RawTextHelpFormatter)
    dp_grid_parser.add_argument('--lonb', metavar='lon0,lon1', required=True, type=lonlat,
                                help="Minimum and maximum longitudes of the geographical grid\n"
                                "before mapping to the displaced pole space\n"
                                "formatted as 'lon0,lon1'. Required.")
    dp_grid_parser.add_argument('--latb', metavar='lat0,lat1', required=True, type=lonlat,
                                help="Minimum and maximum latitudes of the geographical grid\n"
                                "before mapping to the displaced pole space\n"
                                "formatted as 'lat0,lat1'. Required.")
    dp_grid_parser.add_argument('--res', type=float, required=True,
                                help="Target equatorial resolution in degrees before mapping\n"
                                "to the displaced pole space. The resoution will be adapted\n"
                                "in both directions so that the given lonb and latb boundaries\n"
                                "are exactly reached at RHO points. Required.")
    dp_grid_parser.add_argument('--mode', choices=['regular', 'tele'],
                                help="Choice for the type of latitudinal grid spacing before mapping\n"
                                "to the displaced pole space. 'regular' gives a regular lon,lat grid,\n"
                                "'tele' gives a telescopic grid which latitudnal spacing varies as\n"
                                "the cosine of the latitude (like in the longitudinal direction for a\n"
                                "regular lon grid). This makes the grid isotropic BEFORE MAPPING.\n"
                                "Default 'regular'.")
    dp_grid_parser.add_argument('--NP', metavar='lat0,lat1', default=None, type=lonlat,
                                help="Displaced north pole geographical lon lat coordinates\n"
                                "formatted as 'lon,lat'. Default 0,90")
    dp_grid_parser.add_argument('--SP', metavar='lon,lat', default=None, type=lonlat,
                                help="Displaced south pole geographical lon lat coordinates\n"
                                "formatted as 'lon,lat'. Default antipode of NP")
    dp_grid_parser.add_argument('-o', '--out', required=True,
                                help="Path of the output file, required.")

    # Add corners command parser
    # --------------------------
    dsc = """Add cell-corners to ROMS grid file.

This function will be needed for ROMS grid datasets that do not yet
contain the grid cell corners at RHO, U and V points. We use the
dual points (rho<->psi, u<->v) to compute the corner values and
extrapolate linearly for boundary points.
"""
    add_crn_parser = subparsers.add_parser('add_crn', help="Add cell corners to ROMS grid file.",
                                           description=dsc, formatter_class=RawTextHelpFormatter)
    add_crn_parser.add_argument('grd_file', help="Path to the original ROMS grid file.")
    add_crn_parser.add_argument('-o', '--out',
                                help="Path to the output file. If not given, the original\n"
                                "file gets completed.")

    # Run command parser
    # ------------------
    dsc = """Run a ROMSOC configuration on Euler."""

    run_parser = subparsers.add_parser('run', help="Run a ROMSOC configuration.",
                                       description=dsc, formatter_class=RawTextHelpFormatter)
    run_parser.add_argument('param_file', help="Path to the user parameter file containing\n"
                            "modifications to the default params dictionnary")

    # Generate OASIS restart files command parser
    # -------------------------------------------
    dsc = """Generate OASIS restart file(s) from exported exchanged field(s)."""

    oas_rst_parser = subparsers.add_parser('oas_rst', help="Generate OASIS restart file(s).",
                                           description=dsc, formatter_class=RawTextHelpFormatter)
    oas_rst_parser.add_argument('fld_path', type=Path,
                                help="Path to the exported OASIS exchanged field(s).\n"
                                "If pointing to a directory, generate all restart files.\n"
                                "If pointing to a file, generate only the corresponding restart file")
    oas_rst_parser.add_argument('rst_path', type=Path,
                                help="Path to the directory where OASIS restart files are written.")
    oas_rst_parser.add_argument('--time', type=int, default=-1,
                                help="time record to extract from te exported OASIS exchanged field(s).\n"
                                "Default: -1")

    # Generate building scripts
    # -------------------------
    dsc = """Generate build script for OASIS, COSMO and ROMS"""

    bld_parser = subparsers.add_parser('build', help="Generate build script",
                                       description=dsc, formatter_class=RawTextHelpFormatter)
    bld_parser.add_argument('targets', nargs='+',
                            help="list of codes to build. Can be (non case sensitive): all, oasis, cosmo, roms")
    bld_parser.add_argument('--run', '-r', action='store_true',
                            help='launch the build script')
    bld_parser.add_argument('--no_cpl', '-n', action='store_true',
                            help="build non coupled code (will ignore oasis)")

    # Parse arguments and take action
    # ===============================
    args = parser.parse_args()
    
    if args.command == 'aux':
        # Get the ROMSOC dataset
        ROMSOC_ds = get_ROMSOC_ds(args.roms, args.cosmo, args.delta)
        # Write to file
        # - ML - A check should be added to see if the file exists
        ROMSOC_ds.to_netcdf(args.out)

    elif args.command == 'dp_grid':
        # Generate grid dataset
        ROMS_grd_ds = get_dp_grid_ds(args.lonb, args.latb, args.res, args.NP, args.SP, args.mode)
        # Write to file
        ROMS_grd_ds.to_netcdf(args.out)

    elif args.command == 'add_crn':
        # Get ROMS grid dataset completed with cell-corners
        roms_grd = add_corners(args.grd_file)
        # Write to file
        roms_grd.to_netcdf(args.out if args.out is not None else args.grd_file)

    elif args.command == 'run':
        # Prepare input parameters for the run
        params = default_run_params.copy()
        exec(open(args.param_file).read())
        # Prepare run directory and possibly submit
        case = ROMSOC_Run(params)
        case.genJob()

    elif args.command == 'oas_rst':
        # Check arguments
        if not args.fld_path.exists():
            raise ValueError("FLD_PATH must point to an existing file or directory")
        elif not args.rst_path.is_dir():
            raise ValueError("RST_PATH must point to an existing directory")
        # Generate restart file(s)
        gen_oas_rst(args.fld_path, args.rst_path, time_rec=args.time)

    elif args.command == 'build':
        script = build_script(args.targets, run=args.run, coupled=not args.no_cpl)

import os
import logging
import netCDF4

class ROMS_Job:
    """Class for generating text input files for one single ROMS job."""

    def __init__(self,params,jobnr=1,nr_jobs=1):
        # List of keys which need to be present in params:
        klist = ['dt','ndtfast','ninfo','navg','nrpfavg','nrrec','ldefhis',
                 'nwrt','nrpfhis','avgNames','hisNames','rstNames','nrCores','title',
                 'srcDir','grdFile','frcFiles','bryFiles','iniFiles','runDir',
                 'avgOutFolder','hisOutFolder','NT','lsfName','clmFiles']
        keys_missing = []
        # Check if all keys are present in params:
        for k in klist:
            if not k in params:
                keys_missing.append(k)
        if len(keys_missing) > 0:
            if len(keys_missing) == 1:
                msg = 'the following key is missing in parameters dictionary: {}'.format(keys_missing[0])
            else:
                msg = 'the following keys are missing in parameters dictionary: '
                for s in keys_missing:
                    msg += '{} '.format(s)
            raise ValueError(msg)
        # Number of CPU cores to be used for ROMS:
        self._nrCores = params['nrCores']
        # Name of ROMS run (to be used with keyword "title" in text input file):
        self._title = params['title']
        # Source directory for ROMS:
        self._srcDir = params['srcDir']
        # Full path of grid file (string):
        self._grdFile = params['grdFile']
        # Full path(s) of forcing file(s) as a list of strings:
        self._frcFiles = params['frcFiles']
        # Full path to ini file (string):
        self._iniFiles = params['iniFiles']
        # Run directory:
        self._runDirectory = params['runDir']
        if not os.path.exists(self._runDirectory):
            raise ValueError('run directory not found: {}'.format(self._runDirectory))
        # Folder for avg output files:
        self._avgOutFolder = params['avgOutFolder']
        # Folder for his output files:
        self._hisOutFolder = params['hisOutFolder']
        # Dictionary of all parameters:
        self._params = params
        # Job number: used for the text input file name and as index into lists
        # giving some info for each job. Note however that the first job is 1.
        self._jobNr = jobnr
        # Total number of jobs:
        self._nrJobs = nr_jobs
        # Sigma coordinate surface control parameter (will be read from ini file):
        self._theta_s = 0.0
        # Sigma coordinate bottom control parameter (will be read from ini file):
        self._theta_b = 0.0
        # Sigma coordinate surface/bottom layer width (will be read from ini file):
        self._Tcline = 150.0
        # Try to get sigma coordinate parameters:
        if len(self._iniFiles) > 0:
            logging.debug('reading sigma coord params from: {}'.format(self._iniFiles[0]))
            import netCDF4
            nc = netCDF4.Dataset(self._iniFiles[0],'r')
            self._theta_s = nc.variables['theta_s'][0]
            self._theta_b = nc.variables['theta_b'][0]
            self._Tcline = nc.variables['Tcline'][0]
            nc.close()

    def _checkConsistency(self):
        """Do some consistency checks on the parameters."""
        if not self._params['lsfName'] or len(self._params['lsfName']) == 0:
            raise ValueError('job name must be set first')
        # Check if settings related to ini and rst files are consistent:
        if not self._params['iniFiles'] or len(self._params['iniFiles'])==0:
            raise ValueError('no intial files given')
        nr_jobs = self._nrJobs
        if not self._params['rstNames'] and len(self._params['iniFiles'])==nr_jobs\
           and nr_jobs>1:
            for idx in range(nr_jobs):
                rname = 'rst_for_{:03}.nc'.format(idx)
                iname = os.path.basename(self._params['iniFiles'][idx])
                if iname != rname:
                    raise ValueError('inconsistent setting of ini and rst files.')
        if not self._params['rstNames'] and len(self._params['iniFiles'])!=nr_jobs\
           and len(self._params['iniFiles'])!=1:
            raise ValueError('inconsistent setting of ini and rst files.')
        if self._params['rstNames'] and len(self._params['rstNames'])!=nr_jobs:
            raise ValueError('inconsistent setting of rst files.')
        # Check clm settings: self._params['clmFiles'] can be None or of 0 length,
        # or it can contain 1 nonempty list of strings, or it can be a list containing
        # nr_jobs lists of strings:
        if self._params['clmFiles'] is not None:
            if not isinstance(self._params['clmFiles'],list):
                raise ValueError("self._params['clmFiles'] must be a list")
            if len(self._params['clmFiles']) == 0:
                raise ValueError("self._params['clmFiles'] must not be empty")
            for l in self._params['clmFiles']:
                if not isinstance(l,list):
                    raise ValueError("self._params['clmFiles'] must contain string lists")
                for ff in l:
                    if not isinstance(ff,str):
                        raise ValueError("illegal entry in self._params['clmFiles']: {}".format(ff))
            if len(self._params['clmFiles'])!=1 and len(self._params['clmFiles'])!=nr_jobs:
                raise ValueError("length of self._params['clmFiles'] must be of length 1 or the number of jobs")
            for l in self._params['clmFiles']:
                for f in l:
                    if not isinstance(f,str):
                        raise ValueError("elements of self._params['clmFiles'] must be lists of strings")
        # Check bry settings: the same rules apply as for clm files
        if self._params['bryFiles'] is not None:
            if not isinstance(self._params['bryFiles'],list):
                raise ValueError("self._params['bryFiles'] must be a list")
            if len(self._params['bryFiles']) == 0:
                raise ValueError("self._params['bryFiles'] must not be empty")
            for l in self._params['bryFiles']:
                if not isinstance(l,list):
                    raise ValueError("self._params['bryFiles'] must contain string lists")
                for ff in l:
                    if not isinstance(ff,str):
                        raise ValueError("illegal entry in self._params['bryFiles']: {}".format(ff))
            if len(self._params['bryFiles'])!=1 and len(self._params['bryFiles'])!=nr_jobs:
                raise ValueError("length of self._params['bryFiles'] must be of length 1 or the number of jobs")
            for l in self._params['bryFiles']:
                for f in l:
                    if not isinstance(f,str):
                        raise ValueError("elements of self._params['bryFiles'] must be lists of strings")
        # Check settings for frc files: self._params['frcFiles'] must be a list containig at least
        # one sublist containing file names.
        if not isinstance(self._params['frcFiles'],list):
            raise ValueError("self._params['frcFiles'] must be a list")
        if len(self._params['frcFiles']) == 0:
            raise ValueError("self._params['frcFiles'] must not be empty")
        if not isinstance(self._params['frcFiles'][0],list):
            raise ValueError("self._params['frcFiles'] must contain one or more lists")
        if len(self._params['frcFiles'])!=1 and len(self._params['frcFiles'])!=nr_jobs:
                raise ValueError("length of self._params['frcFiles'] must be of length 1 or the number of jobs")
        for l in self._params['frcFiles']:
            for f in l:
                if not isinstance(f,str):
                    raise ValueError("elements of self._params['frcFiles'] must be lists of strings")
        # Check if the user has set nwrt, navg and nrst correctly: either one single integer or
        # a list of integers (one for each job)
        slist = ['nrst', 'navg','nwrt']
        for k in slist:
            if self._params[k] is None:
                raise ValueError("{} values not specified".format(k))
            if not isinstance(self._params[k],int):
                if not isinstance(self._params[k],list):
                    raise ValueError("self._params[{}] must be a single integer or a list of integers".format(k))
                if len(self._params[k]) != nr_jobs:
                    raise ValueError("if self._params[{}] is a list, then it must contain one value per job".format(k))        

    def genTextInput(self):
        """Generate text input file."""
        # Run consistency checks first:
        self._checkConsistency()
        # Change to run directory:
        os.chdir(self._runDirectory)
        if self._jobNr is None:
            fname = '{}.in'.format(self._params['lsfName'])
        else:
            fname = '{}_{:03}.in'.format(self._params['lsfName'],self._jobNr)
        # Open text input file, erasing it if it exists:
        f = open(fname,'w')
        f.write('title:\n')
        f.write('   {}\n\n'.format(self._title))
        f.write('time_stepping: NTIMES   dt[sec]  NDTFAST  NINFO\n')
        dt = self._params['dt'] # 3d time step, will be used for nwrt, navg, nrst
        f.write('{:20} {:8} {:8} {:8}\n\n'.format(self._params['ntimes'],dt,
            self._params['ndtfast'],self._params['ninfo']))
        f.write('S-coord: THETA_S,   THETA_B,    TCLINE (m)\n')
        f.write('{:17.1F} {:10.1F} {:10.1F}\n\n'.format(self._theta_s,self._theta_b,self._Tcline))
        f.write('grid:  filename\n')
        f.write('   {}\n\n'.format(self._grdFile))
        f.write('forcing: filename\n')
        if len(self._frcFiles) == 1:
            frc_files = self._frcFiles[0]
        else:
            frc_files = self._frcFiles[self._jobNr-1]
        for ff in frc_files:
            f.write('   {}\n'.format(ff))
        f.write('\n')
        f.write('initial: NRREC  filename\n')
        f.write('{:12}\n'.format(self._params['nrrec']))
        if len(self._iniFiles) == 1:
            if self._jobNr == 1:
                f.write('   {}\n\n'.format(self._iniFiles[0]))
            elif self._params['rstNames'] and len(self._params['rstNames'])>1:
                # User has given names for rst files:
                f.write('   {}\n\n'.format(self._params['rstNames'][self._jobNr-2]))
            else:
                # No name for rst file give: take default
                f.write('   {}/rst_for_{:03}.nc\n\n'.format(self._params['rstOutFolder'],self._jobNr))
        else:
            f.write('   {}\n\n'.format(self._iniFiles[self._jobNr-1]))
        # Check if at least one clm file is used: 
        if not self._params['clmFiles'] or len(self._params['clmFiles'])==0:
            clm_present = False
        else:
            clm_present = True
        if clm_present:
            f.write('climatology:\n')
            if len(self._params['clmFiles']) == 1:
                clm_files = self._params['clmFiles'][0]
            else:
                clm_files = self._params['clmFiles'][self._jobNr-1]
            f.write('{:6}\n'.format(len(clm_files)))
            for ff in clm_files:
                f.write('   {}\n'.format(ff))
            f.write('\n')
        # Check if at least one bry file is used:
        if not self._params['bryFiles'] or len(self._params['bryFiles'])==0:
            bry_present = False
        else:
            bry_present = True
        if bry_present:
            f.write('boundary:\n')
            if len(self._params['bryFiles']) == 1:
                bry_files = self._params['bryFiles'][0]
            else:
                bry_files = self._params['bryFiles'][self._jobNr-1]
            f.write('{:6}\n'.format(len(bry_files)))
            for ff in bry_files:
                f.write('   {}\n'.format(ff))
            f.write('\n')
        # Restart file:
        f.write('restart:          NRST, NRPFRST / filename\n')
        if isinstance(self._params['nrst'],list):
            nrst = self._params['nrst'][self._jobNr-1] // dt
        else:
            nrst = self._params['nrst'] // dt
        f.write('{:20} {:5}\n'.format(nrst,self._params['nrpfrst']))
        if self._params['rstNames']:
            rstName = self._params['rstNames'][self._jobNr-1]
        else:
            rstName = 'rst_for_{:03}.nc'.format(self._jobNr+1)
        f.write('     {}/{}\n\n'.format(self._params['rstOutFolder'],rstName))
        # History output:
        f.write('history: LDEFHIS, NWRT, NRPFHIS / filename\n')
        nwrt = self._params['nwrt'][self._jobNr-1] // dt
        f.write('{:>6} {:15} {:5}\n'.format(self._params['ldefhis'],nwrt,
                                             self._params['nrpfhis']))
        if self._params['hisNames']:
            hisName = self._params['hisNames'][self._jobNr-1]
        else:
            hisName = 'his_{:03}.nc'.format(self._jobNr)
        f.write('     {}/{}\n\n'.format(self._params['hisOutFolder'],hisName))
        # Avg output:
        f.write('averages: NTSAVG, NAVG, NRPFAVG / filename\n')
        navg = self._params['navg'][self._jobNr-1] // dt
        f.write('{:13} {:8} {:5}\n'.format(1,navg,
                                             self._params['nrpfavg']))
        if self._params['avgNames']:
            avgName = self._params['avgNames'][self._jobNr-1]
        else:
            avgName = 'avg_{:03}.nc'.format(self._jobNr)
        f.write('     {}/{}\n\n'.format(self._params['avgOutFolder'],avgName))
        # Primary his fields:
        f.write('primary_history_fields: zeta UBAR VBAR  U  V   wrtT(1:NT)\n')
        f.write(' '*26)
        f.write('T    T    T   T  T   ')  # zeta UBAR VBAR  U  V
        s = 'T '*self._params['NT']
        f.write(s[:-1]+'\n\n')
        # Primary averages:
        f.write('primary_averages: zeta UBAR VBAR  U  V   wrtT(1:NT)\n')
        f.write(' '*20)
        f.write('T    T    T   T  T   ')  # zeta UBAR VBAR  U  V
        s = 'T '*self._params['NT']
        f.write(s[:-1]+'\n\n')
        # Auxiliary history fields:
        f.write('auxiliary_history_fields:  rho Omega  W  Akv  Akt  Rich RichN Aks  HBL\n')
        f.write('                            F   T     F   F    T    T     T    T    T\n\n')
        # Auxiliary averages:
        f.write('auxiliary_averages: rho Omega  W  Akv  Akt  Rich RichN Aks  HBL\n')
        f.write('                     F   T     F   F    T    T     T    T    T\n\n')
        # rho0:
        f.write('rho0:\n')
        f.write('{:12.1F}\n\n'.format(1000))
        f.write('lateral_visc:   VISC2,\n')
        f.write('{:20.1F}\n\n'.format(0))
        # Background tracer diffusion:
        f.write('tracer_diff2: TNU2(1:NT)           [m^2/sec for all]\n')
        s = ' '*14 + '0. '*self._params['NT']
        f.write(s[:-1]+'\n\n')
        # Bottom drag, etc:
        f.write('bottom_drag:     RDRG [m/s],  RDRG2,  Zob [m],  Cdb_min, Cdb_max\n')
        f.write('                 3.0E-04      0.d0     0.E-4     1.E-4    1.E-2\n\n')
        f.write('gamma2:\n')
        f.write('                 1.d0\n\n')
        f.write('sponge:          X_SPONGE [m],    V_SPONGE [m^2/sec]\n')
        f.write('                   200.e3           200.\n\n')
        f.write('nudg_cof: TauM2_in/out  attnM2   TauM3_in/out  TauT_in/out [days for all]\n')
        f.write('             360    360    0.002        360 360      360  360\n')
        f.close()
        
        # Generate romsoc.nml namelist if parameters are present
        if self._params['romsoc']:
            with open('romsoc.nml', 'w') as f:
                f.write(" &romsoc\n")
                f.write("  IOASISDEBUGLVL={:d},\n".format(self._params['romsoc']['OASIS_dbg']))
                f.write("  romsoc_aux_name='{:}'\n".format(self._params['romsoc']['romsoc_aux_name']))
                f.write("  l_oas_seq={:}\n".format(self._params['romsoc']['l_oas_seq']))
                f.write(" /\n")

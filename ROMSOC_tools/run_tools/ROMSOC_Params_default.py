# Dictionnary of all possible user defined ROMSOC parameters
# initilaized with default values. User defined values are to be given
# in a dedicated python file provided by the user to the command line
# interface.
#
# Author: Damian Loher, Dec 2018
# Modifications: Matthieu Leclair, Feb 2020

_to_be_set_ = 'setting required'
_roms_to_be_set_ = 'ROMS setting required'
_cosmo_to_be_set_ = 'COSMO setting required'
_romsoc_to_be_set_ = 'ROMSOC setting required'

params = dict()

# Main parameters (not component specific)
# ========================================
params['main'] = dict()
# LSF name of job of this run:
params['main']['lsfName']= 'ROMSOC'
# Start dates of each job of this run: list containing entries like '1998010100'.
# 2 dates result in one job being sumbitted to LSF
params['main']['dateList']  = _to_be_set_
# In case not the whole range of dates have to be ran (job number starts at 1)
params['main']['firstJob'] = None
params['main']['lastJob'] = None
# Run directory (which then also assigns params['cosmo']['runDir'] and params['roms']['rundir'])
params['main']['runDir'] = _to_be_set_
# Whether the run directory is to be purged before job 1 is run (has not effect on jobs
# with number >1):
params['main']['purgeRunDir'] = True
# Wall time to be used in the BSUB script: string 'hh:mm'
params['main']['walltime'] = _to_be_set_
# Name of the job for which the first job needs to wait (if needed, keep at None otherwise)
params['main']['firstJobWait'] = None
# Only run ROMS
params['main']['runROMSonly'] = False
# Only run COSMO
params['main']['runCOSMOonly'] = False
# Submit run or just prepare run directory and let user submit by hand
params['main']['submit'] = True

# ROMS parameters
# ===============
params['roms'] = dict()
# Options related to entries in text input file:
params['roms']['title'] = 'ROMSOC'
# 3D time step (s)
params['roms']['dt'] = _roms_to_be_set_
params['roms']['ndtfast'] = _roms_to_be_set_
# Frequency of ROMS diagnostic messages.
params['roms']['ninfo'] = _roms_to_be_set_
# Frequencies for writing avg files : either a list of 1 integer (navg identical
# for all jobs) or a list of nr_jobs integers (navg for each job).
params['roms']['navg'] = _roms_to_be_set_
params['roms']['nrpfavg'] = 0
params['roms']['nrrec'] = 0  # for ini file
params['roms']['ldefhis'] = 'T'
# Frequencies for writing his files : either a list of 1 integer (nwrt identical
# for all jobs) or a list of nr_jobs integers (nwrt for each job).
params['roms']['nwrt'] = _roms_to_be_set_
params['roms']['nrpfhis'] = 0
# Restart frequencies: either a list of 1 integer (nrst identical for all jobs)
# or a list of nr_jobs integers (nrst for each job).
params['roms']['nrst'] = None
params['roms']['nrpfrst'] = 0
# Names of rst files: either a list giving the file name for each run
# or None (then default names rst_for_002.nc, rst_for_003.nc, etc will be used).
params['roms']['rstNames'] = None
# Folder where rst files will be created:
params['roms']['rstOutFolder'] = './roms_output'
# Names of avg files: either a list giving the file name for each run
# or None (then default names avg_001.nc, avg_002.nc, etc will be used).
params['roms']['avgNames'] = None
# Folder where avg files will be created:
params['roms']['avgOutFolder'] = './roms_output'
# Names of his files: either a list giving the file name for each run
# or None (then default names his_001.nc, his_002.nc, etc will be used).
params['roms']['hisNames'] = None
# Folder where his files will be created:
params['roms']['hisOutFolder'] = './roms_output'
# number of tracers (NT in ROMS code)
params['roms']['NT'] = 10
# ROMS grid file
params['roms']['grdFile'] = _roms_to_be_set_
# Number of cores dedicated to ROMS
params['roms']['nrCores'] = _roms_to_be_set_
# Input folder
params['roms']['inputFolder'] = _roms_to_be_set_
# Initial files: There are 2 possibilities:
# - a file name can be set for each job (so a list of nr_jobs strings needs to be given)
# - one single file name can be set, which will be used for the first job
#   only. The other jobs will use rst_for_002.nc, rst_for_003.nc, etc
params['roms']['iniFiles'] = _roms_to_be_set_
# List of ROMS forcing file(s). Two different ways are possible:
# - a list containing one list of paths to the frc file(s): then this set
#   of files will be used for all jobs
# - a list containing nr_jobs lists of strings (where nr_jobs is the number of jobs).
#   This allow users to specify different frc files for each job
params['roms']['frcFiles'] = _roms_to_be_set_
# Settings for bry and clm files: if bry files are used, then params['roms']['clmFiles'] should be
# set to None,  and the other way round if clim files are used.
# bry and clm files can be specified in two ways:
# - a list containing one list of path names: then this set of files will be used
#   for all jobs
# - a list containing nr_jobs lists of path names: this is needed if different sets
#   of bry or clm files have to be used for each job
params['roms']['bryFiles'] = _roms_to_be_set_
params['roms']['clmFiles'] = _roms_to_be_set_
# ROMS src code folder (where the executable is to be found)
params['roms']['srcDir'] = _roms_to_be_set_

# ROMSOC specific parameters
params['roms']['romsoc'] = dict()
# print debug level
params['roms']['romsoc']['OASIS_dbg'] = _romsoc_to_be_set_
# ROMSOC auxiliary file path
params['roms']['romsoc']['romsoc_aux_name'] = _romsoc_to_be_set_
# Run in sequential mode.
# Mainly for debug purposes or generating of oasis restart files
# (in combination with EXPOUT status in namcouple)
params['roms']['romsoc']['l_oas_seq'] = _romsoc_to_be_set_

# Cosmo parameters
# ================
params['cosmo'] = dict()
# Path of folder containing the laf files for Cosmo.
params['cosmo']['inputFolder'] = _cosmo_to_be_set_
params['cosmo']['exec'] = _cosmo_to_be_set_

# Parameters for namelists:
params['cosmo']['INPUT_ORG'] = dict()
params['cosmo']['INPUT_ORG']['startlon_tot'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['startlat_tot'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['pollat'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['pollon'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['polgam'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['dlon'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['dlat'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['ie_tot'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['je_tot'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['ke_tot'] = _cosmo_to_be_set_
# Initial date of forecast. Default taken from first entry in dateList
params['cosmo']['INPUT_ORG']['ydate_ini'] = None
# Start date of the forecast from which the boundary files are used.
# Default taken from first entry in dateList
params['cosmo']['INPUT_ORG']['ydate_bd'] = None
params['cosmo']['INPUT_ORG']['dt'] = _cosmo_to_be_set_  # time step (s)
params['cosmo']['INPUT_ORG']['nprocx'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['nprocy'] = _cosmo_to_be_set_
params['cosmo']['INPUT_ORG']['nboundlines'] = 3
params['cosmo']['INPUT_ORG']['idbg_level'] = 2

params['cosmo']['INPUT_IO'] = dict()
# Folder where restart files will be created. The full path of the
# run directory will be prepended to this folder.
params['cosmo']['INPUT_IO']['ydir_restart'] = 'cosmo_output/restart/'
# Triple 'nhour_restart' in namelist IOCTL. If None, restart files produced for each subjob.
params['cosmo']['INPUT_IO']['nhour_restart'] = None
params['cosmo']['INPUT_IO']['ydirini'] = 'cosmo_input/'
params['cosmo']['INPUT_IO']['ydirbd'] = 'cosmo_input/'
# Lists of variables to be written to Netcdf files:
params['cosmo']['INPUT_IO']['GRIBOUT'] = dict()
l1 = ['U','V','W','T','P','POT_VORTIC','PS','PMSL','QV','QC','QI','HPBL',
      'T_S','T_2M','TD_2M','TCH','TCM','CLCT','CLCH','CLCM','CLCL',
      'BAS_CON','TOP_CON','HBAS_CON','HTOP_CON','FC','TKVM','TKVH',
      'U_10M','V_10M']
l2 = ['ASHFL_S','ALHFL_S','AUMFL_S','AVMFL_S','ASOB_S','ATHB_S','TOT_PREC',
      'RAIN_GSP','SNOW_GSP','RAIN_CON','SNOW_CON','PRR_CON','PRS_CON',
      'PRR_GSP','PRS_GSP','AUSTRSSO','AVSTRSSO']
params['cosmo']['INPUT_IO']['GRIBOUT']['varlist'] = [l1, l2]
params['cosmo']['INPUT_IO']['GRIBOUT']['ysuffix'] = ['', 'BND']
params['cosmo']['INPUT_IO']['GRIBOUT']['hcomb'] = dict()
# Llist of 2 elements for both output streams: starting hour, end hour, frequency in hours
params['cosmo']['INPUT_IO']['GRIBOUT']['hcomb']['start'] = _cosmo_to_be_set_
params['cosmo']['INPUT_IO']['GRIBOUT']['hcomb']['end'] = _cosmo_to_be_set_
params['cosmo']['INPUT_IO']['GRIBOUT']['hcomb']['freq'] = _cosmo_to_be_set_
params['cosmo']['INPUT_IO']['GRIBOUT']['lwrite_const'] = [ '.TRUE.', '.FALSE.' ]
# Folder(s) where output files will be written (relative to run directory):
params['cosmo']['INPUT_IO']['GRIBOUT']['ydir'] = [ 'cosmo_output/', 'cosmo_output/' ]

params['cosmo']['INPUT_DIA'] = dict()
params['cosmo']['INPUT_DIA']['lgplong'] = '.FALSE.'

# Parameters for INPUT_OASIS namelist
params['cosmo']['INPUT_OASIS'] = dict()
params['cosmo']['INPUT_OASIS']['debug_oasis'] = 3

# Oasis parameters 
# ================
params['oasis'] = dict()
# Folder containing restart and grid files:
params['oasis']['rstDir'] = _romsoc_to_be_set_
params['oasis']['rstFiles'] = ['SST.nc','ustr_u.nc','vstr_u.nc','ustr_v.nc','vstr_v.nc',
                               'nhf.nc','swr.nc','tep.nc']
params['oasis']['grdFiles'] = ['areas.nc','grids.nc','masks.nc']
params['oasis']['rmpDir'] = _romsoc_to_be_set_
params['oasis']['rmpFiles'] = ['rmp_coas_to_Rrho_DISTWGT.nc', 'rmp_coau_to_R__u_DISTWGT.nc',
                               'rmp_coau_to_R__v_DISTWGT.nc', 'rmp_coav_to_R__u_DISTWGT.nc',
                               'rmp_coav_to_R__v_DISTWGT.nc', 'rmp_Rrho_to_coas_DISTWGT.nc']
params['oasis']['cfTableFile'] = _romsoc_to_be_set_

params['oasis']['namcouple'] = dict()
params['oasis']['namcouple']['NLOGPRT'] = 0
params['oasis']['namcouple']['dbg'] = 2

# Coupling field properties
# lags automatically set following params['roms']['romsoc']['l_oas_seq']
fields = 'sst', 'ustru', 'vstru', 'ustrv', 'vstrv', 'nhf', 'swr', 'tep'
frq = _romsoc_to_be_set_
status = 'EXPORTED'   # EXPOUT to output exchanged fields to netcdf files
params['oasis']['namcouple']['coupfreq'] = {fld: frq for fld in fields}
params['oasis']['namcouple']['status'] = {fld: status for fld in fields}

# Sizes of the different grids:
params['oasis']['namcouple']['Rrho_dims'] = _romsoc_to_be_set_
params['oasis']['namcouple']['Ru_dims'] = _romsoc_to_be_set_
params['oasis']['namcouple']['Rv_dims'] = _romsoc_to_be_set_
params['oasis']['namcouple']['coas_dims'] = _romsoc_to_be_set_

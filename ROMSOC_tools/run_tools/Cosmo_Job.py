import os
import logging

# Class for generating text input files for Cosmo
# Author: DL, 2018

class Cosmo_Job:
    """Class for generating text input files for one single Cosmo job."""

    def __init__(self,params,jobnr=1):
        # Dictionary of all parameters:
        self._params = params
        # Job number (note that the first job is 1):
        self._jobNr = jobnr

    def genTextInput(self):
        """Generate namelist input files."""
        # Change to run directory:
        os.chdir(self._params['runDir'])
        self._genInputORG(self._params['INPUT_ORG'])
        self._genInputIO(self._params['INPUT_IO'])
        self._genInputDYN()
        self._genInputPHY()
        self._genInputDIA(self._params['INPUT_DIA'])
        self._genInputINI()
        self._genInputASS()
        self._genInputOASIS(self._params['INPUT_OASIS'])

    def _genInputORG(self,params):
        """Write file INPUT_ORG."""
        fname = 'INPUT_ORG_{:03}'.format(self._jobNr)
        f = open(fname,'w')
        f.write(' &LMGRID\n')
        f.write('  startlon_tot={}, startlat_tot={},\n'.format(params['startlon_tot'],
                        params['startlat_tot']))
        f.write('  pollat={}, pollon={}, polgam={},\n'.format(params['pollat'],
                        params['pollon'],params['polgam']))
        f.write('  dlon={}, dlat={},\n'.format(params['dlon'],params['dlat']))
        f.write('  ie_tot={}, je_tot={}, ke_tot={},\n'.format(params['ie_tot'],
                        params['je_tot'],params['ke_tot']))
        f.write(' /\n')
        f.write(' &RUNCTL\n')
        f.write('  ydate_ini=\'{}\',\n'.format(params['ydate_ini']))
        f.write('  ydate_bd=\'{}\',\n'.format(params['ydate_bd']))
        f.write('  dt={:.1F},\n'.format(params['dt']))
        f.write('  hstart={:.1F}, hstop={:.1F},\n'.format(params['hstart'],params['hstop']))
        f.write('  idbg_level={:d},\n'.format(params['idbg_level']))
        f.write('  lreproduce=.TRUE., luseobs=.FALSE.,\n')
        f.write('  lphys=.TRUE., ldiagnos=.TRUE., ldfi=.FALSE.,\n')
        f.write('  luse_rttov=.FALSE.,\n')
        f.write('  nprocx={}, nprocy={}, nprocio=0,\n'.format(params['nprocx'],params['nprocy']))
        f.write('  nboundlines={}, lreorder=.FALSE.,\n'.format(params['nboundlines']))
        f.write('  ldatatypes=.FALSE.,\n')
        f.write('  ncomm_type=1,\n')
        f.write(' /\n')
        f.write(' &TUNING\n')
        f.write('  clc_diag=0.5, rat_can=1.0,\n')
        f.write('  c_soil=1.0,\n')
        f.write('  wichfakt=0.0, crsmin=150., qc0=0.0, qi0=0.0,\n')
        f.write('  rat_sea=20.0, rat_lam=1.0, rlam_heat=1.0,\n')
        f.write(' /\n')
        f.close()

    def _genInputIO(self,params):
        """Write file INPUT_IO."""
        fname = 'INPUT_IO_{:03}'.format(self._jobNr)
        f = open(fname,'w')
        f.write(' &IOCTL\n')
        f.write('  ngribout=2, lasync_io=.FALSE.,\n')
        f.write('  lbdclim=.TRUE.,\n')
        f.write('  yform_read=\'ncdf\',\n')
        f.write('  ydir_restart=\'{}/{}\',\n'.format(self._params['runDir'],params['ydir_restart']))
        f.write('  ytunit_restart=\'d\',\n')
        f.write('  nhour_restart={},\n'.format(params['nhour_restart']))
        f.write(' /\n')
        f.write(' &DATABASE\n')
        f.write(' /\n')
        f.write(' &GRIBIN\n')
        f.write('  ydirini=\'{}/{}\',\n'.format(self._params['runDir'],params['ydirini']))
        f.write('  lchkini=.TRUE.,\n')
        f.write('  lana_qi=.TRUE., lana_qr_qs=.FALSE., lana_rho_snow=.FALSE.,\n')
        f.write('  ydirbd=\'{}/{}\',\n'.format(self._params['runDir'],params['ydirbd']))
        f.write('  hincbound=6.0,\n')
        f.write('  lchkbd=.TRUE.,\n')
        f.write('  llb_qi=.TRUE., llb_qr_qs=.FALSE.,\n')
        f.write('  lbdana=.TRUE.,\n')
        f.write('  lan_t_s=.FALSE.,\n')
        f.write('  lan_t_so0=.TRUE., lan_t_cl=.TRUE., lan_w_cl=.TRUE.,\n')
        f.write('  lan_vio3=.TRUE., lan_hmo3=.TRUE., lan_plcov=.TRUE., lan_lai=.TRUE.,\n')
        f.write('  lan_rootdp=.TRUE., lan_t_snow=.TRUE., lan_w_i=.TRUE.,\n')
        f.write('  lan_w_snow=.TRUE.,\n')
        f.write('  lan_rho_snow=.TRUE.,\n')
        f.write(' /\n')
        nr_grbout = len(params['GRIBOUT']['varlist'])
        for i in range(nr_grbout):
            f.write(' &GRIBOUT\n')
            f.write('  yform_write=\'ncdf\',\n')
            f.write('  hcomb={:.1F},{:.1F},{:.1F}\n'.format(params['GRIBOUT']['hcomb']['start'][i],
                        params['GRIBOUT']['hcomb']['end'][i], 
                        params['GRIBOUT']['hcomb']['freq'][i]))
            f.write('  yvarml=')
            for v in params['GRIBOUT']['varlist'][i]:
                f.write('\'{}\','.format(v))
            f.write('\n')
            f.write('  yvarpl=\' \',\n')
            f.write('  yvarzl=\' \',\n')
            f.write('  yvarsl=\' \',\n')
            f.write('  lcheck=.TRUE.,\n')
            f.write('  lwrite_const={},\n'.format(params['GRIBOUT']['lwrite_const'][i]))
            f.write('  ydir=\'{}/{}\',\n'.format(self._params['runDir'],params['GRIBOUT']['ydir'][i]))
            f.write('  ytunit=\'d\',\n')
            f.write('  ysuffix=\'{}\',\n'.format(params['GRIBOUT']['ysuffix'][i]))
            f.write(' /\n')
        f.close()

    def _genInputDYN(self):
        """Write file INPUT_DYN."""
        fname = 'INPUT_DYN_{:03}'.format(self._jobNr)
        f = open(fname,'w')
        f.write(' &DYNCTL\n')
        f.write('  l2tls=.TRUE., irunge_kutta=2, irk_order=3, iadv_order=5,\n')
        f.write('  y_scalar_advect=\'Bott2_Strang\',\n')
        f.write('  y_vert_adv_dyn=\'impl2\', ieva_order=3,\n')
        f.write('  ldiabf_lh=.TRUE.,\n')
        f.write('  lsemi_imp=.FALSE., lcond=.TRUE.,\n')
        f.write('  lspubc=.TRUE., lexpl_lbc=.TRUE., ldyn_bbc=.TRUE.,\n')
        f.write('  betasw=0.4, xkd=0.1, epsass=0.15,\n')
        f.write('  lhordiff=.TRUE., itype_hdiff=2,\n')
        f.write('  hd_dhmax=250.0, itype_bbc_w=114,\n')
        f.write('  itype_fast_waves=2, divdamp_slope=20.0, itype_outflow_qrsg=1, rlwidth=85000.0,\n')
        f.write(' /\n')
        f.close()

    def _genInputPHY(self):
        """Write file INPUT_PHY."""
        fname = 'INPUT_PHY_{:03}'.format(self._jobNr)
        f = open(fname,'w')
        f.write(' &PHYCTL\n')
        f.write('  lgsp=.TRUE., itype_gscp=3, ldiniprec=.FALSE.,\n')
        f.write('  lrad=.TRUE., hincrad=1.0, nradcoarse=1,lemiss=.FALSE.,icldm_rad=4,\n')
        f.write('  ltur=.TRUE., itype_turb=3, ninctura=1, imode_turb=1,\n')
        f.write('     itype_tran=4, imode_tran=1,\n')
        f.write('     lexpcor=.FALSE., ltmpcor=.FALSE., lprfcor=.FALSE.,\n')
        f.write('     lnonloc=.FALSE., lcpfluc=.FALSE., limpltkediff=.TRUE.,\n')
        f.write('     itype_wcld=2, icldm_rad=4, icldm_turb=2, icldm_tran=0,\n')
        f.write('     itype_synd=2,\n')
        f.write('  lsoil=.TRUE., itype_evsl=2, itype_trvg=2,\n')
        f.write('     lmulti_layer=.TRUE., lmelt=.TRUE., lmelt_var=.TRUE.,\n')
        f.write('     ke_soil=7,lmulti_snow=.FALSE.,\n')
        f.write('     czml_soil = 0.005, 0.02, 0.06, 0.18, 0.54, 1.62, 4.86, 14.58,\n')
        f.write('  lconv=.TRUE., lcape=.FALSE., lctke=.FALSE., nincconv=4, itype_conv=0,\n')
        f.write('  lforest=.TRUE., llake=.FALSE., lseaice=.FALSE., lsso=.TRUE., shflmult=1.0, lhflmult=1.0,\n')
        f.write(' /\n')
        f.close()

    def _genInputDIA(self,params):
        """Write file INPUT_DIA."""
        fname = 'INPUT_DIA_{:03}'.format(self._jobNr)
        f = open(fname,'w')
        f.write(' &DIACTL\n')
        f.write('  n0gp=0,      hincgp=1.0,\n')
        f.write('  n0meanval=0, nincmeanval=1,\n')
        f.write('  lgplong={}, lgpshort=.FALSE., lgpspec=.FALSE.,\n'.format(params['lgplong']))
        f.write(' /\n')
        f.close()

    def _genInputINI(self):
        """Write file INPUT_INI."""
        fname = 'INPUT_INI_{:03}'.format(self._jobNr)
        f = open(fname,'w')
        f.write(' &INICTL\n')
        f.write('  ndfi=1, nfilt=1,\n')
        f.write('  tspan=3600.0, taus=3600.0,\n')
        f.write('  dtbak=40.0, dtfwd=40.0,\n')
        f.write(' /\n')
        f.close()

    def _genInputASS(self):
        """Write file INPUT_ASS."""
        fname = 'INPUT_ASS_{:03}'.format(self._jobNr)
        f = open(fname,'w')
        f.write(' &NUDGING\n')
        f.write('  lnudge=.FALSE.,\n')
        f.write(' /\n')
        f.close()

    def _genInputOASIS(self, params):
        """Write file INPUT_OASIS."""
        fname = 'INPUT_OASIS_{:03}'.format(self._jobNr)
        f = open(fname,'w')
        f.write(' &OASISCTL\n')
        f.write('  ytype_oce=\'romsO\',\n')
        f.write('  debug_oasis={:d},\n'.format(params['debug_oasis']))
        f.write(' /\n')
        f.close()

import xarray as xr
from pathlib import Path


# Correspondance dictionnary between restart file
# and exchange OASIS fields output by the model
# (EXPOUT option in namcouple)
rst_fld = {'RA_SST_O_cosmoc_01.nc': 'SST.nc',
           'RO_UST_U_ROMSOC_02.nc': 'ustr_u.nc',
           'RO_VST_U_ROMSOC_03.nc': 'vstr_u.nc',
           'RO_UST_V_ROMSOC_04.nc': 'ustr_v.nc',
           'RO_VST_V_ROMSOC_05.nc': 'vstr_v.nc',
           'RO_NHF_A_ROMSOC_06.nc': 'nhf.nc',
           'RO_SWR_A_ROMSOC_07.nc': 'swr.nc',
           'RO_TEP_A_ROMSOC_08.nc': 'tep.nc'}


def gen_oas_rst(fld_path, rst_path, time_rec=-1):
    """Generate oasis restart files from exported exchanged fields."""

    # directory case
    if fld_path.is_dir():
        for oas_fld, rst_file in rst_fld.items():
            if not (fld_path/oas_fld).exists():
                raise ValueError("{} not found in {}".format(oas_fld, fld_path))
            else:
                extract_time_rec(fld_path/oas_fld, rst_path/rst_file, time_rec)
    # file case
    else:
        oas_fld = fld_path.name()
        if oas_fld not in fld_rst:
            raise ValueError("{0} is not a valid OASIS exported exchange field."
                             "Must be one of {1}".format(oas_fld, list(fld_rst.keys())))
        else:
            extract_time_rec(fld_path, rst_path/fld_rst[oas_fld], time_rec)


def extract_time_rec(in_path, out_path, time_rec=-1):
    """Extract time record of in_path to out_path"""
    with xr.open_dataset(in_path) as ds_in:
        print("Extracting time {:d} from {} to {}".format(time_rec, in_path.name, out_path.name))
        if out_path.exists():
            print('{} will be overwritten'.format(out_path))
        ds_in[{'time': time_rec}].to_netcdf(out_path, mode='w')
    

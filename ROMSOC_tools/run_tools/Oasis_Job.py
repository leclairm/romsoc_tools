import os
import logging

class Oasis_Job:
    """Class for generating namcouple files."""

    def __init__(self,params,jobnr=1,copy_rstfiles=False):
        # Dictionary of all parameters:
        self._params = params
        # Job number (note that the first job is 1):
        self._jobNr = jobnr
        # Whether restart files are to be copied:
        self._copyRST = copy_rstfiles

    def prepRun(self):
        # Change to run directory:
        os.chdir(self._params['runDir'])
        # Copy restart and grid files, if necessary:
        if self._copyRST:
            for f in (self._params['rstFiles']+self._params['grdFiles']):
                fsrc = '{}/{}'.format(self._params['rstDir'],f)
                if not os.path.exists(f) and os.path.exists(fsrc):
                    os.system('cp -p {} .'.format(fsrc))
        # Create Links to remap files:
        for f in self._params['rmpFiles']:
            fsrc = '{}/{}'.format(self._params['rmpDir'],f)
            if not os.path.exists(f) and os.path.exists(fsrc):
                os.symlink(fsrc,f)
        # Copy cf_name_table.txt:
        if not os.path.exists('cf_name_table.txt'):
            os.system('cp -p {} .'.format(self._params['cfTableFile']))
        # Generate namcouple file:
        self._genNamcouple(self._params['namcouple'])

    def _genNamcouple(self,params):
        """Write namcouple file."""
        fname = 'namcouple_{:03}'.format(self._jobNr)
        f = open(fname,'w')
        f.write('#---------------------------\n')
        f.write('# This is a typical input file for OASIS3_MCT, using netCDF format\n')
        f.write('# for restart input files.\n')
        f.write('#\n')
        f.write('# According to the Oasis user\'s guide, the namcouple is a text file\n')
        f.write('# with the following characteristics:\n')
        f.write('#\n')
        f.write('#  - the keywords used to separate the information can appear in any order;\n')
        f.write('#  - the number of blanks between two character strings is non-significant;\n')
        f.write('#  - all lines beginning with # are ignored and considered as comments;\n')
        f.write('#  - blank lines are supported, but only since OASIS3-MCT 4.0 version.\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('  $NFIELDS\n')
        f.write('# Put the number of fields exchanged by OASIS.\n')
        f.write('# If it is bigger than twice the number of fields exchanged, put also on\n')
        f.write('# the same line the maximum number of prism_def_var_proto\n')
        f.write('# called by one component model in the coupled system (optional).\n')
        f.write('# For the definition of the fields, see under $STRINGS keyword\n')
        f.write('#\n')
        f.write('             8\n')
        f.write('  $END\n')
        f.write('#########################################################################\n')
        f.write('  $RUNTIME\n')
        f.write('    {}\n'.format(params['RUNTIME']))
        f.write('  $END\n')
        f.write('#########################################################################\n')
        f.write('  $NLOGPRT\n')
        f.write('# Index of printing level in output file cplout: 0 = no printing\n')
        f.write('#  1 = main routines and field names when treated, 2 = complete output\n')
        f.write('   {}\n'.format(params['NLOGPRT']))
        f.write('  $END\n')
        f.write('#########################################################################\n')
        f.write('  $STRINGS\n')
        f.write('#\n')
        f.write('# The above variables are the general parameters for the experiment.\n')
        f.write('# Everything below has to do with the fields being exchanged.\n')
        f.write('#\n')
        f.write('# Field 1 :\n')
        f.write('#\n')
        f.write('#   First line:\n')
        f.write('# 1) and 2) Symbolic names for the field before and after interpolation\n')
        f.write('#           (8 characters maximum)\n')
        f.write('# 3) Index of field in cf_name_table.txt\n')
        f.write('# 4) Exchange frequency for the field in seconds\n')
        f.write('# 5) Number of analysis to be performed\n')
        f.write('# 6) Restart input NetCDF file names\n')
        f.write('# 7) Field status (EXPORTED, AUXILARY, IGNORED, EXPOUT, IGNOUT, INPUT, OUTPUT)\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('#                      OCEAN  --->>>  ATMOS\n')
        f.write('#                      --------------------\n')
        f.write('#########################################################################\n')
        f.write('#\n')
        f.write('SO_SST_A RA_SST_O {} {} 3 SST.nc {}\n'.format(params['dbg'], params['coupfreq']['sst'],
                                                               params['status']['sst']))
        f.write('{} {} {} {} Rrho coas LAG={:+}\n'.format(params['Rrho_dims'][0], params['Rrho_dims'][1],
                                                          params['coas_dims'][0], params['coas_dims'][1],
                                                          params['lag']['sst']))
        f.write('R 0 R 0\n')
        f.write('LOCTRANS SCRIPR BLASNEW\n')
        f.write('#\n')
        f.write('AVERAGE\n')
        f.write('DISTWGT LR SCALAR LATLON 1 1\n')
        f.write('1.0 1\n')
        f.write('CONSTANT 273.15\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('#                      ATMOS  --->>>  OCEAN\n')
        f.write('#                      --------------------\n')
        f.write('#########################################################################\n')
        f.write('#\n')
        f.write('SA_UST_U RO_UST_U {} {} 2 ustr_u.nc {}\n'.format(params['dbg'], params['coupfreq']['ustru'],
                                                                    params['status']['ustru']))
        f.write('{} {} {} {} coau R__u LAG={:+}\n'.format(params['coas_dims'][0], params['coas_dims'][1],
                                                          params['Ru_dims'][0], params['Ru_dims'][1],
                                                          params['lag']['ustru']))
        f.write('R 0 R 0\n')
        f.write('LOCTRANS  SCRIPR\n')
        f.write('#\n')
        f.write('AVERAGE\n')
        f.write('DISTWGT LR SCALAR LATLON 1 1\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('#\n')
        f.write('SA_VST_U RO_VST_U {} {} 2 vstr_u.nc {}\n'.format(params['dbg'], params['coupfreq']['vstru'],
                                                                    params['status']['vstru']))
        f.write('{} {} {} {} coav R__u LAG={:+}\n'.format(params['coas_dims'][0], params['coas_dims'][1],
                                                          params['Ru_dims'][0], params['Ru_dims'][1],
                                                          params['lag']['vstru']))
        f.write('R 0 R 0\n')
        f.write('LOCTRANS  SCRIPR\n')
        f.write('#\n')
        f.write('AVERAGE\n')
        f.write('DISTWGT LR SCALAR LATLON 1 1\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('#\n')
        f.write('SA_UST_V RO_UST_V {} {} 2 ustr_v.nc {}\n'.format(params['dbg'], params['coupfreq']['ustrv'],
                                                                    params['status']['ustrv']))
        f.write('{} {} {} {} coau R__v LAG={:+}\n'.format(params['coas_dims'][0], params['coas_dims'][1],
                                                          params['Rv_dims'][0], params['Rv_dims'][1],
                                                          params['lag']['ustrv']))
        f.write('R 0 R 0\n')
        f.write('LOCTRANS  SCRIPR\n')
        f.write('#\n')
        f.write('AVERAGE\n')
        f.write('DISTWGT LR SCALAR LATLON 1 1\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('#\n')
        f.write('SA_VST_V RO_VST_V {} {} 2 vstr_v.nc {}\n'.format(params['dbg'], params['coupfreq']['vstrv'],
                                                                    params['status']['vstrv']))
        f.write('{} {} {} {} coav R__v LAG={:+}\n'.format(params['coas_dims'][0], params['coas_dims'][1],
                                                          params['Rv_dims'][0], params['Rv_dims'][1],
                                                          params['lag']['vstrv']))
        f.write('R 0 R 0\n')
        f.write('LOCTRANS  SCRIPR\n')
        f.write('#\n')
        f.write('AVERAGE\n')
        f.write('DISTWGT LR SCALAR LATLON 1 1\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('#\n')
        f.write('SA_NHF_O RO_NHF_A {} {} 2 nhf.nc {}\n'.format(params['dbg'], params['coupfreq']['nhf'],
                                                                 params['status']['nhf']))
        f.write('{} {} {} {} coas Rrho LAG={:+}\n'.format(params['coas_dims'][0], params['coas_dims'][1],
                                                          params['Rrho_dims'][0], params['Rrho_dims'][1],
                                                          params['lag']['nhf']))
        f.write('R 0 R 0\n')
        f.write('LOCTRANS  SCRIPR\n')
        f.write('#\n')
        f.write('AVERAGE\n')
        f.write('DISTWGT LR SCALAR LATLON 1 1\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('#\n')
        f.write('SA_SWR_O RO_SWR_A {} {} 2 swr.nc {}\n'.format(params['dbg'], params['coupfreq']['swr'],
                                                                 params['status']['swr']))
        f.write('{} {} {} {} coas Rrho LAG={:+}\n'.format(params['coas_dims'][0], params['coas_dims'][1],
                                                          params['Rrho_dims'][0], params['Rrho_dims'][1],
                                                          params['lag']['swr']))
        f.write('R 0 R 0\n')
        f.write('LOCTRANS  SCRIPR\n')
        f.write('#\n')
        f.write('AVERAGE\n')
        f.write('DISTWGT LR SCALAR LATLON 1 1\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('#\n')
        f.write('SA_TEP_O RO_TEP_A {} {} 3 tep.nc {}\n'.format(params['dbg'], params['coupfreq']['tep'],
                                                                 params['status']['tep']))
        f.write('{} {} {} {} coas Rrho LAG={:+}\n'.format(params['coas_dims'][0], params['coas_dims'][1],
                                                          params['Rrho_dims'][0], params['Rrho_dims'][1],
                                                          params['lag']['tep']))
        # - ML - Is this really necessary ?
        f.write('R 0 R 0\n')
        f.write('LOCTRANS SCRIPR BLASNEW\n')
        f.write('#\n')
        f.write('AVERAGE\n')
        f.write('DISTWGT LR SCALAR LATLON 1 1\n')
        f.write('8640.0 0\n')
        f.write('# Changing to cm/day\n')
        f.write('#\n')
        f.write('#########################################################################\n')
        f.write('#\n')
        f.write('  $END\n')
        f.close()

# ---------------------------------------------------------------------- #
#                             *DISCLAIMER*                               #
#                                                                        #
#   This file is for example/test puproses only. It can be used as a     #
#   basis for new configurations but user modifications should not be    #
#   version-controled in *this* repository.                              #
#                                                                        #
#   Note that all available user parameters are not set here, some are   #
#   kept to their default values. They can all be checked in             #
#   ROMSOC_Params_default.py                                             #
# ---------------------------------------------------------------------- #

# Parameters for the California Current System setting with identical
# domains for COSMO and ROMS and doubled resolution for ROMS.

# Main parameters (not component specific)
# ========================================
# LSF name of job of this run:
params['main']['lsfName']= 'ROMSOC_CalCS2001_Aug2018_ROMSdoubled'
# Start dates of each job of this run: list containing entries like '1998010100'.
# 2 dates result in one job being sumbitted to LSF
params['main']['dateList']  = ['2001010100', '2001010200']
# Run directory (which then also assigns params['cosmo']['runDir'] and params['roms']['rundir'])
params['main']['runDir'] = '/cluster/scratch/leclairm/ROMSOC/ROMSOC_CalCS2001_Aug2018_ROMSdoubled'
# Wall time to be used in the BSUB script: string 'hh:mm'
params['main']['walltime'] = '24:00'

# ROMS parameters
# ===============
# Options related to entries in text input file:
params['roms']['title'] = 'CalCS2001'
# 3D time step (s)
params['roms']['dt'] = 300
params['roms']['ndtfast'] = 42
# Frequency of ROMS diagnostic messages.
params['roms']['ninfo'] = 100
# Frequencies for writing avg files : either a list of 1 integer (navg identical
# for all jobs) or a list of nr_jobs integers (navg for each job).
params['roms']['navg'] = 24*3600
# Frequencies for writing his files : either a list of 1 integer (nwrt identical
# for all jobs) or a list of nr_jobs integers (nwrt for each job).
params['roms']['nwrt'] = 9000000000
# Restart frequencies: either a list of 1 integer (nrst identical for all jobs)
# or a list of nr_jobs integers (nrst for each job).
params['roms']['nrst'] = None
# ROMS grid file
params['roms']['grdFile'] = './roms_input/ROMS_CalCS_grd_crn.nc'
# Number of cores dedicated to ROMS
params['roms']['nrCores'] = 96
# Input folder
params['roms']['inputFolder'] = '/nfs/kryo/work/leclairm/COSMOR/ROMSOC_2001/ROMS_input_Aug2018_ROMSdoubled_64lev'
# Initial files: There are 2 possibilities:
# - a file name can be set for each job (so a list of nr_jobs strings needs to be given)
# - one single file name can be set, which will be used for the first job
#   only. The other jobs will use rst_for_002.nc, rst_for_003.nc, etc
params['roms']['iniFiles'] = ['./roms_input/CoSMOR_CalCS_ini.nc']
# List of ROMS forcing file(s). Two different ways are possible:
# - a list containing one list of paths to the frc file(s): then this set
#   of files will be used for all jobs
# - a list containing nr_jobs lists of strings (where nr_jobs is the number of jobs).
#   This allow users to specify different frc files for each job
params['roms']['frcFiles'] = [ ['{}/CoSMOR_CalCS_frc_2001.nc'.format(params['roms']['inputFolder']) ] ]
# Settings for bry and clm files: if bry files are used, then params['roms']['clmFiles'] should be
# set to None,  and the other way round if clim files are used.
# bry and clm files can be specified in two ways:
# - a list containing one list of path names: then this set of files will be used
#   for all jobs
# - a list containing nr_jobs lists of path names: this is needed if different sets
#   of bry or clm files have to be used for each job
params['roms']['clmFiles'] = [ ['{}/CoSMOR_CalCS_clm.nc'.format(params['roms']['inputFolder'])] ]
params['roms']['bryFiles'] = None
# ROMS src code folder (where the roms executable is to be found)
params['roms']['srcDir'] = '/cluster/home/leclairm/COSMOR/roms_src_ethz/src'

# ROMSOC specific parameters
# print debug level
params['roms']['romsoc']['OASIS_dbg'] = 1
# ROMSOC auxiliary file path
params['roms']['romsoc']['romsoc_aux_name'] = './roms_input/CalCS_ROMSOC_aux.nc'
# Run in sequential mode for oasis restart file production
# (in combination with EXPOUT status in namcouple)
params['roms']['romsoc']['l_oas_seq'] = '.TRUE.'

# Cosmo parameters
# ================
# Path of folder containing the laf files for Cosmo.
params['cosmo']['inputFolder'] = '/nfs/malva/work/loher/COSMOR/int2lm_output/USWC_gridAug2018_2001'
params['cosmo']['exec'] = '/cluster/home/leclairm/COSMOR/cosmo_MCT/'

# Parameters for namelists:
params['cosmo']['INPUT_ORG']['startlon_tot'] = -39.91
params['cosmo']['INPUT_ORG']['startlat_tot'] = -23.91
params['cosmo']['INPUT_ORG']['pollat'] = 41.5
params['cosmo']['INPUT_ORG']['pollon'] = 83.0
params['cosmo']['INPUT_ORG']['polgam'] = 0.0
params['cosmo']['INPUT_ORG']['dlon'] = 0.09
params['cosmo']['INPUT_ORG']['dlat'] = 0.09
params['cosmo']['INPUT_ORG']['ie_tot'] = 358
params['cosmo']['INPUT_ORG']['je_tot'] = 358
params['cosmo']['INPUT_ORG']['ke_tot'] = 40
params['cosmo']['INPUT_ORG']['dt'] = 60  # time step (s)
params['cosmo']['INPUT_ORG']['nprocx'] = 8
params['cosmo']['INPUT_ORG']['nprocy'] = 12

# Lists of variables to be written to Netcdf files:
l1 = ['U','V','W','T','P','POT_VORTIC','PS','PMSL','QV','QC','QI','HPBL',
      'T_S','T_2M','TD_2M','TCH','TCM','CLCT','CLCH','CLCM','CLCL',
      'BAS_CON','TOP_CON','HBAS_CON','HTOP_CON','FC','TKVM','TKVH',
      'U_10M','V_10M']
l2 = ['ASHFL_S','ALHFL_S','AUMFL_S','AVMFL_S','ASOB_S','ATHB_S','TOT_PREC',
      'RAIN_GSP','SNOW_GSP','RAIN_CON','SNOW_CON','PRR_CON','PRS_CON',
      'PRR_GSP','PRS_GSP','AUSTRSSO','AVSTRSSO']
params['cosmo']['INPUT_IO']['GRIBOUT']['varlist'] = [l1, l2]
params['cosmo']['INPUT_IO']['GRIBOUT']['ysuffix'] = ['', 'BND']
# Llist of 2 elements for both output streams: starting hour, end hour, frequency in hours
params['cosmo']['INPUT_IO']['GRIBOUT']['hcomb']['start'] = [ 0, 0 ]
params['cosmo']['INPUT_IO']['GRIBOUT']['hcomb']['end'] = [ 8760, 8760 ]
params['cosmo']['INPUT_IO']['GRIBOUT']['hcomb']['freq'] = [ 6, 6 ]
params['cosmo']['INPUT_IO']['GRIBOUT']['lwrite_const'] = [ '.TRUE.', '.FALSE.' ]

# Parameters for INPUT_OASIS namelist
params['cosmo']['INPUT_OASIS']['debug_oasis'] = 3

# Oasis parameters 
# ================
# Folder containing restart and grid files:
params['oasis']['rstDir'] = '/nfs/kryo/work/leclairm/COSMOR/ROMSOC_2001/OASIS_input_gridAug2018_ROMSdoubled'
params['oasis']['rstFiles'] = ['SST.nc','ustr_u.nc','vstr_u.nc','ustr_v.nc','vstr_v.nc',
                               'nhf.nc','swr.nc','tep.nc']
params['oasis']['grdFiles'] = ['areas.nc','grids.nc','masks.nc']
params['oasis']['rmpDir'] = '/nfs/kryo/work/leclairm/COSMOR/ROMSOC_2001/OASIS_input_gridAug2018_ROMSdoubled'
params['oasis']['rmpFiles'] = ['rmp_coas_to_Rrho_DISTWGT.nc', 'rmp_coau_to_R__u_DISTWGT.nc',
                               'rmp_coau_to_R__v_DISTWGT.nc', 'rmp_coav_to_R__u_DISTWGT.nc',
                               'rmp_coav_to_R__v_DISTWGT.nc', 'rmp_Rrho_to_coas_DISTWGT.nc']
params['oasis']['cfTableFile'] = '/nfs/malva/work/loher/COSMOR/oasis_input/cf_name_table.txt'

# Coupling field properties
# lags automatically set following params['roms']['romsoc']['l_oas_seq']
fields = 'sst','ustru','vstru','ustrv','vstrv','nhf','swr','tep'
params['oasis']['namcouple']['coupfreq'] = {fld: 600 for fld in fields}
params['oasis']['namcouple']['status'] = {fld: 'EXPOUT' for fld in fields}

# Sizes of the different grids:
params['oasis']['namcouple']['Rrho_dims'] = [ 703, 703 ]
params['oasis']['namcouple']['Ru_dims'] = [ 702, 703 ]
params['oasis']['namcouple']['Rv_dims'] = [ 703, 702 ]
params['oasis']['namcouple']['coas_dims'] = [ 358, 358 ]

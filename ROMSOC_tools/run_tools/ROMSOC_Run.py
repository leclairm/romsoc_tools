import os
import datetime
import logging
from .ROMSOC_Params_default import _to_be_set_, _roms_to_be_set_, _cosmo_to_be_set_, _romsoc_to_be_set_
from .ROMS_Job import ROMS_Job
from .Cosmo_Job import Cosmo_Job
from .Oasis_Job import Oasis_Job

class ROMSOC_Run:
    """
    Class for settig up ROMSOC runs. Creates the input files for Oasis,
    Cosmo and ROMS based on the user's specifications.
    """

    def __init__(self, params):
        # Dictionary containing all the parameters:
        self._params = params.copy()
        self.check_params()

        # General settings
        # ----------------
        self.broadcastParams()
        self.setDates()
        self.set_nrCores()

        # ROMS settings
        # -------------
        self.checkROMSIniFiles()
        self.checkROMSOutput()

        # COSMO settings
        # --------------
        self.setCosmoYdate()
        self.checkCosmoHcomb()
        self.checkCosmoNhourRestart()

        # ROMSOC settings
        # ---------------
        self.set_cpl_mode()
            
        # Placeholder flags for storing options set by user
        # -------------------------------------------------
        # Some items need to be saved since they are changed by _prepROMS and _prepCosmo:
        self._flags = dict()


    def check_params(self):
        """Check if any required parameter isn't set"""
        
        def check_unset(sub_dict, key_path='', unset_keys=[]):
            """Recursive function to look for unset keys in the dictionnary tree"""
            
            for key, val in sub_dict.items():
                new_key_path = key_path + '[' + key + ']'
                if isinstance(val, dict):
                    check_unset(val, key_path=new_key_path, unset_keys=unset_keys)
                elif val == _to_be_set_ \
                     or val == _roms_to_be_set_ and not self._runCOSMOonly \
                     or val == _cosmo_to_be_set_ and not self._runROMSonly \
                     or val == _romsoc_to_be_set_ and not (self._runROMSonly or self._runCOSMOonly):
                    unset_keys += [new_key_path]
            return unset_keys

        unset_params = check_unset(self._params)
        if unset_params:
            print("The following required parameters are unset:")
            for p in unset_params:
                print('params' + p)
                

    def broadcastParams(self):
        """Broadcast some parameters from the main section."""
        
        self._params['roms']['runDir'] = self._params['main']['runDir']
        self._params['cosmo']['runDir'] = self._params['main']['runDir']
        self._params['oasis']['runDir'] = self._params['main']['runDir']
        self._params['roms']['lsfName'] = self._params['main']['lsfName']
        self._runROMSonly = self._params['main']['runROMSonly']
        self._runCOSMOonly = self._params['main']['runCOSMOonly']

        
    def setDates(self):
        """Set list of job dates and first and last job indices."""

        # Set job dates
        if len(self._params['main']['dateList']) < 2:
            raise ValueError('at least 2 dates must be given (start and end of job)')
        jobStartDates = []
        for d in self._params['main']['dateList']:
            if isinstance(d,str):
                if len(d) == 10:
                    yr = int(d[:4])
                    mm = int(d[4:6])
                    dd = int(d[6:8])
                    hh = int(d[8:10])
                    jobStartDates.append( datetime.datetime(yr,mm,dd,hh) )
                else:
                    raise ValueError('invalid date: {}'.format(d))
            else:
                raise ValueError('dates must be given as strings yyyymmddhh')
        self._params['jobStartDates'] = jobStartDates

        # Set first job number
        nr = self._params['main']['firstJob']
        if nr is not None:
            if not isinstance(nr,int):
                raise ValueError('argument of setFirstJob must be an integer')
            if nr <= 0:
                raise ValueError('argument of setFirstJob must be positive')
            if self._params['jobStartDates']:
                njobs = len(self._params['jobStartDates'])-1
                if nr > njobs:
                    msg = 'number of first job must be smaller then '
                    msg += 'the total number of jobs: {}'.format(njobs)
                    raise ValueError(msg)
            self._firstJob = nr
            # Do not purge output folder if the number of the first job is >1:
            if nr > 1:
                self._params['main']['purgeRunDir'] = False
        else:
            self._firstJob = 1

        # Set last job number
        nr = self._params['main']['lastJob']
        if nr is not None:
            if not isinstance(nr,int):
                raise ValueError('argument of setLastJob must be an integer')
            if nr <= 0:
                raise ValueError('argument of setLastJob must be positive')
            if self._params['jobStartDates']:
                njobs = len(self._params['jobStartDates'])-1
                if nr > njobs:
                    msg = 'number of last job must be smaller then '
                    msg += 'the total number of jobs: {}'.format(njobs)
                    raise ValueError(msg)
            self._lastJob = nr
        else:
            self._lastJob = len(jobStartDates)-1
                

    def set_nrCores(self):
        """Assign cores to ROMS and COSMO"""
        
        if self._runROMSonly:
            self._params['cosmo']['nrCores'] = 0
        else:
            self._params['cosmo']['nrCores'] = \
                self._params['cosmo']['INPUT_ORG']['nprocx'] \
                * self._params['cosmo']['INPUT_ORG']['nprocy']
            if self._runCOSMOonly:
                self._params['roms']['nrCores'] = 0
            

    def setCosmoYdate(self):
        """Sets the initial date of the Cosmo forecast.
        Argument "date" is of the form yyyymmddhh.
        """
        
        date_0 = self._params['main']['dateList'][0]
        if self._params['cosmo']['INPUT_ORG']['ydate_ini'] is None:
            self._params['cosmo']['INPUT_ORG']['ydate_ini'] = date_0
        # - ML - Maybe rather use self._params['main']['dateList'][self._firstJob-1] for 'ydate_bd'
        if self._params['cosmo']['INPUT_ORG']['ydate_bd'] is None:
            self._params['cosmo']['INPUT_ORG']['ydate_bd'] = date_0

            
    def checkCosmoNhourRestart(self):
        """Check triple 'nhour_restart' in namelist IOCTL."""
        
        rstlist = self._params['cosmo']['INPUT_IO']['nhour_restart']
        if rstlist is not None:
            err_msg = 'argument of setCosmoNhourRestart must be a list of 3 integers'
            list_ok = True
            try:
                list_ok = len(rstList) == 3
            except:
                list_ok = False
            if not list_ok:
                raise ValueError(err_msg)
            if rstList[1] < rstList[0]:
                raise ValueError('stop time must be larger than start time')

            
    def checkCosmoHcomb(self):
        """Check if hcomb is correctly set for both gribout blocks."""
        
        for key, vlist in self._params['cosmo']['INPUT_IO']['GRIBOUT']['hcomb'].items():
            param_str = "params['cosmo']['INPUT_IO']['GRIBOUT']['hcomb']['{:s}']".format(key)
            err_msg = "{:s} must be a list of 2 ints or floats".format(param_str)
            vlist_ok = True
            try:
                vlist_ok = len(vlist) == 2 and \
                    isinstance(vlist[0],(int, float)) and isinstance(vlist[1],(int, float))
            except:
                vlist_ok = False
            if not vlist_ok:
                raise ValueError(err_msg)

            
    def checkROMSIniFiles(self):
        """Set list of ini files. There are 2 possibilities:
          - a file name can be set for each job
          - one single file name can be set, which will be used for the
            first job only. The other jobs will use rst_for_002.nc,
            rst_for_003.nc, etc
        """
        
        list_ok = True
        nr_jobs = len(self._params['jobStartDates'])-1
        err_msg = "list of initial files must have one single entry or " \
            "the same number of entries as there are jobs: {}".format(nr_jobs)
        ini_list = self._params['roms']['iniFiles']
        try:
            list_ok = len(ini_list) == 1 or len(ini_list) == nr_jobs
        except:
            list_ok = False
        if not list_ok:
                raise ValueError(err_msg)
            

    def checkROMSOutput(self):
        """Check the list of avg, his and rst files"""
        
        nr_jobs = len(self._params['jobStartDates'])-1
        for key in 'avgNames', 'hisNames', 'rstNames':
            lst = self._params['roms'][key]
            if lst is not None:
                list_ok = True
                err_msg = "params['roms']['{:s}'] must have one single entry or " \
                    "the same number of entries as there are jobs: {}".format(key, nr_jobs)
                try:
                    list_ok = len(lst) == nr_jobs
                except:
                    list_ok = False
                if not list_ok:
                    raise ValueError(err_msg)

        for key in 'navg', 'nwrt', 'nrst':
            n_chk = self._params['roms'][key]
            if n_chk is not None:
                n_chk_ok = True
                err_msg = "params['roms'][{:s}] must either be an integer or "\
                    "an list of as many int as there are jobs: {}".format(key, nr_jobs)
                if type(n_chk) == int:
                    self._params['roms'][key] = [n_chk] * nr_jobs
                else:
                    try:
                        n_chk_ok = len(n_chk) == nr_jobs
                    except:
                        n_chk_ok = False
                    if not n_chk_ok:
                        raise ValueError(err_msg)

    def set_cpl_mode(self):
        """Adjust lag in namcouple to the coupling mode: concurrent vs sequential"""
        
        if self._params['roms']['romsoc']['l_oas_seq'].lower() == '.true.':
            self._params['oasis']['namcouple']['lag'] = \
                {fld: 0 for fld in self._params['oasis']['namcouple']['coupfreq'].keys()}
        else:
            self._params['oasis']['namcouple']['lag'] = self._params['oasis']['namcouple']['coupfreq']

        
    def _saveFlags(self):
        """Save some info about defaults set by the user. These are first stored in
        self._params but will be overwritten by _prepROMS and _prepCosmo.
        """
        
        self._flags['default_nrst'] = (self._params['roms']['nrst'] is None)
        self._flags['default_nhour_restart'] = (self._params['cosmo']['INPUT_IO']['nhour_restart'] is None)

        
    def _prepRunDir(self):
        """Does some basic taks in the run directory, e.g. creates symlinks,
        copies executables, removes existing files.
        """
        
        # Change to run directory and earase its contents, if required:
        os.chdir(self._params['main']['runDir'])
        if self._params['main']['purgeRunDir']:
            logging.debug('clearing run directory')
            os.system('rm -fr ./*')
        # Create subfolders:
        if not self._runROMSonly:
            if not os.path.isdir('cosmo_output'):
                os.mkdir('cosmo_output')
                logging.debug('folder "cosmo_output" created')
            if not os.path.isdir('cosmo_output/restart'):
                logging.debug('folder "cosmo_output/restart" created')
                os.mkdir('cosmo_output/restart')
        if (not self._runCOSMOonly) and (not os.path.isdir('roms_output')):
            os.mkdir('roms_output')
            logging.debug('folder "roms_output" created')
        # Create symlinks, if necessary:
        if (not self._runCOSMOonly) and (not os.path.exists('roms_input')):
            # Now the symlink either does not exist or is broken. Remove it if it exists:
            if os.path.lexists('roms_input'):
                # Symlink exists and is broken:
                os.remove('roms_input')
            os.symlink(self._params['roms']['inputFolder'],'roms_input')
            logging.debug('symlink "roms_input" created')
        if (not self._runROMSonly) and (not os.path.exists('cosmo_input')):
            # Now the symlink either does not exist or is broken. Remove it if it exists:
            if os.path.lexists('cosmo_input'):
                os.remove('cosmo_input')
            os.symlink(self._params['cosmo']['inputFolder'],'cosmo_input')
        # Copy executables:
        if (not self._runROMSonly) and (not os.path.exists('cosmo')):
            os.system('cp -p {}/cosmo .'.format(self._params['cosmo']['exec']))
        if (not self._runCOSMOonly) and (not os.path.exists('roms')):
            os.system('cp -p {}/roms .'.format(self._params['roms']['srcDir']))

            
    def _prepROMS(self,jobnr):
        """Prepares ROMS text input file in the run folder for multiple jobs."""
        
        logging.debug('called')
        # Set parameters common to all jobs:
        self._params['roms']['runDir'] = self._params['main']['runDir']
        #self._params['roms']['lsfName'] = self._lsfJobName
        nr_jobs = len(self._params['jobStartDates'])-1
        # Get length of job in seconds:
        tdelta = self._params['jobStartDates'][jobnr] - self._params['jobStartDates'][jobnr-1]
        total_sec = tdelta.total_seconds()
        # Set parameters for this job:
        self._params['roms']['ntimes'] = int(total_sec / self._params['roms']['dt'])
        if self._flags['default_nrst']:
            self._params['roms']['nrst'] = int(total_sec)
        # Write ROMS text input file:
        robj = ROMS_Job(self._params['roms'],jobnr,nr_jobs)
        robj.genTextInput()

        
    def _prepCosmo(self,jobnr):
        """Prepares Cosmo text input files in the run folder for multiple jobs."""
        
        logging.debug('called')
        # Get length of job in hours:
        tdelta = self._params['jobStartDates'][jobnr] - self._params['jobStartDates'][jobnr-1]
        total_h = int(tdelta.total_seconds() / 3600.0)
        tdelta = self._params['jobStartDates'][jobnr-1] - self._params['jobStartDates'][0]
        hstart = int(tdelta.total_seconds() / 3600.0)
        hstop = hstart + total_h
        # Set parameters for this job:
        self._params['cosmo']['INPUT_ORG']['hstart'] = hstart
        self._params['cosmo']['INPUT_ORG']['hstop'] = hstop
        if self._flags['default_nhour_restart']:
            self._params['cosmo']['INPUT_IO']['nhour_restart'] = '{},{},{}'.format(hstop,hstop,total_h)
        # Write Cosmo text input files:
        cobj = Cosmo_Job(self._params['cosmo'],jobnr)
        cobj.genTextInput()

        
    def _prepOasis(self,jobnr):
        """Prepares Oasis input files in the run folder for multiple jobs."""
        
        logging.debug('called')
        # Get length of job in seconds:
        tdelta = self._params['jobStartDates'][jobnr] - self._params['jobStartDates'][jobnr-1]
        total_sec = tdelta.total_seconds()
        # Set parameters for this job:
        self._params['oasis']['namcouple']['RUNTIME'] = int(total_sec + \
                self._params['oasis']['namcouple']['lag']['sst'])
        # Write Oasis text input file:
        cprst = (jobnr == 1)
        oobj = Oasis_Job(self._params['oasis'],jobnr,cprst)
        oobj.prepRun()

        
    def _prepRunscripts(self,jobnr):
        # Write bsub script:
        nlists = ['ASS','DIA','DYN','INI','IO','OASIS','ORG','PHY']
        fout = '{}_{:03}.out'.format(self._params['main']['lsfName'],jobnr)
        fin  = '{}_{:03}.in'.format(self._params['main']['lsfName'],jobnr)
        f = open('{}_{:03}.bsub'.format(self._params['main']['lsfName'],jobnr), 'w')
        f.write('#!/bin/sh\n')
        f.write('#BSUB -G es_grube\n')
        f.write('#BSUB -W {}     # Max compute time (wall-clock, hh:mm)\n'.format(self._params['main']['walltime']))
        f.write('#BSUB -n {}      # Number of processors\n'.format(self._params['roms']['nrCores']\
             +self._params['cosmo']['nrCores']))
        f.write('#BSUB -o {}_{:03}.stdout # stdout\n'.format(self._params['main']['lsfName'],jobnr))
        f.write('#BSUB -e {}_{:03}.stderr # stderr\n'.format(self._params['main']['lsfName'],jobnr))
        f.write('#BSUB -J {}_{:03}        # job name\n'.format(self._params['main']['lsfName'],jobnr))
        # Dependency condition:
        if jobnr == self._firstJob and self._params['main']['firstJobWait']:
            f.write('#BSUB -w done({})\n'.format(self._params['main']['firstJobWait']))
        if jobnr != self._firstJob:
            f.write('#BSUB -w done({}_{:03})\n'.format(self._params['main']['lsfName'],jobnr-1))
        f.write('\n')
        if not self._runROMSonly:
            f.write('rm -f INPUT_ASS INPUT_DIA INPUT_DYN INPUT_INI INPUT_IO\n')
            f.write('rm -f INPUT_OASIS INPUT_ORG INPUT_PHY\n')
            f.write('rm -f YUCHKDAT YUPRHUMI YUPRMASS YUSPECIF YUTIMING\n')
            for s in nlists:
                f.write('ln -s INPUT_{0}_{1:03} INPUT_{0}\n'.format(s,jobnr))
        if not self._runROMSonly and not self._runCOSMOonly:
            f.write('rm -f debug.notroot.01 debug.notroot.02 debug.root.01 debug.root.02 nout.000000\n')
            f.write('rm -f M_*N namcouple\n')
            f.write('ln -s namcouple_{:03} namcouple\n'.format(jobnr))
            for frst in self._params['oasis']['rstFiles']:
                # Copy restart file xxx.nc to xxx_for_jobnr.nc:
                frst_nr = '{}_for_{:03}.nc'.format(frst[:-3],jobnr)
                f.write('cp -fp {} {}\n'.format(frst,frst_nr))
        f.write('\n')
        f.write('# Modules and lib path:\n')
        f.write('echo "sourcing $HOME/COSMOR/load_modules_intel.sh" > {}\n'.format(fout))
        f.write('source $HOME/COSMOR/load_modules_intel.sh\n')
        f.write('echo >> {}\n'.format(fout))
        f.write('echo "Modules loaded:" >> {}\n'.format(fout))
        f.write('module li >> {} 2>&1\n'.format(fout))
        f.write('echo >> {}\n'.format(fout))
        f.write('export LD_LIBRARY_PATH=$HOME/COSMOR/eccodes-2.7.3-install/lib:$LD_LIBRARY_PATH\n')
        f.write('\n')
        f.write('echo >> {}\n'.format(fout))
        f.write('echo Dir: `pwd` >> {}\n'.format(fout))
        f.write('date >> {}\n'.format(fout))
        f.write('echo ---- Before calling mpirun --- >> {}\n'.format(fout))
        f.write('echo >> {}\n'.format(fout))
        if self._runROMSonly:
            f.write('mpirun ./roms {} >> {}\n'.format(fin,fout))
        elif self._runCOSMOonly:
            f.write('mpirun ./cosmo >> {}\n'.format(fout))
        else:
            f.write('mpirun -np {} ./roms {} : -np {} ./cosmo >> {}\n'.format(\
                self._params['roms']['nrCores'],fin,self._params['cosmo']['nrCores'],fout))
        f.write('echo >> {}\n'.format(fout))
        f.write('echo ---- After mpirun --- >> {}\n'.format(fout))
        f.write('date >> {}\n'.format(fout))
        f.close()

        
    def _submitJob(self,jobnr):
        cmd = 'bsub < {}_{:03}.bsub'.format(self._params['main']['lsfName'],jobnr)
        logging.info('execute "{}"'.format(cmd))
        # Submit the job:
        os.system(cmd)

        
    def genJob(self):
        """Generates all files in the run directory. No job is submitted."""
        
        if self._runROMSonly and self._runCOSMOonly:
            raise ValueError('at most one of self._runROMSonly and self._runCOSMOonly may be True')
        if not os.path.exists(self._params['main']['runDir']):
            print('rundirectory not reachable: {}'.format(self._params['main']['runDir']))
            return
        # Change to run directory:
        os.chdir(self._params['main']['runDir'])
        self._prepRunDir()
        # Loop over jobs:
        for jobnr in list(range(1,len(self._params['jobStartDates']))):
            if jobnr == 1:
                # Save some info about user's settings which will be overwritten
                # by _prepROMS or _prepCosmo:
                self._saveFlags()
            # Remove LSF files which may be left from an earlier try of this job:
            fstdout = '{}_{:03}.stdout'.format(self._params['main']['lsfName'],jobnr)
            if os.path.exists(fstdout):
                os.remove(fstdout)
            fstderr = '{}_{:03}.stderr'.format(self._params['main']['lsfName'],jobnr)
            if os.path.exists(fstderr):
                os.remove(fstderr)
            # Remove .out file, if present:
            fout = '{}_{:03}.out'.format(self._params['main']['lsfName'],jobnr)
            if os.path.exists(fout):
                os.remove(fout)
            # If this job is not to be run, we continue:
            if jobnr<self._firstJob or jobnr>self._lastJob:
                continue
            if self._runROMSonly:
                self._prepROMS(jobnr)
            elif self._runCOSMOonly:
                self._prepCosmo(jobnr)
            else:
                self._prepROMS(jobnr)
                self._prepCosmo(jobnr)
                self._prepOasis(jobnr)
            self._prepRunscripts(jobnr)
            if self._params['main']['submit']:
                self._submitJob(jobnr)

import xarray as xr
import numpy as np
from argparse import ArgumentParser, RawTextHelpFormatter


def add_corners(roms_grd):
    """
    Add corners at RHO, U and V points to the ROMS grid dataset.

    This function will be needed for ROMS grid datasets that do not
    contain the grid cell corners at RHO, U and V points yet. We use
    the dual points (rho<->psi, u<->v) to compute the corner values
    and extrapolate for boundary points.
 
    Parameters
    ----------
    roms_grd : xarray.Dataset or str
        ROMS grid dataset that will be completed with corner values.
        If a string is given, the function will try to get an
        xarray.Dataset from that location.

    Returns
    -------
    roms_grd : xarray.Dataset
        If the roms_grd input is a string, the completed dataset is
        returned, otherwise the input dataset is completed.

    """

    if type(roms_grd) is str:
        return_ds = True
        roms_grd = xr.open_dataset(roms_grd)

    def _fill_bnd(P, *dims):
        """Fill boundary values in given dimensions with linear extrapolation."""

        if len(dims) not in (1,2) or not all(d in ('xi', 'eta') for d in dims):
            raise ValueError("dims must be of length 1 or 2 with elements in ('xi', 'eta')")

        for d in dims:
            if d == 'xi':
                P[:,:, 0] = 2.0 * P[:,:, 1] - P[:,:, 2]
                P[:,:,-1] = 2.0 * P[:,:,-2] - P[:,:,-3]
            elif d == 'eta':
                P[:, 0,:] = 2.0 * P[:, 1,:] - P[:, 2,:]
                P[:,-1,:] = 2.0 * P[:,-2,:] - P[:,-3,:]


    for pt in ('rho', 'u', 'v'):

        # Compute dual extended values
        lon_pt = roms_grd['lon_{:s}'.format(pt)]
        eta, xi = lon_pt.shape
        P_dual = np.empty((2, eta+1, xi+1), np.float64)
        P_crn = np.empty((4, 2, eta, xi), np.float64)

        if pt == 'rho':
            P_dual[0,1:-1,1:-1] = roms_grd['lon_psi'].values
            P_dual[1,1:-1,1:-1] = roms_grd['lat_psi'].values
            _fill_bnd(P_dual, 'xi', 'eta')
        elif pt == 'u':
            P_dual[0,1:-1,:] = roms_grd['lon_v'].values
            P_dual[1,1:-1,:] = roms_grd['lat_v'].values
            _fill_bnd(P_dual, 'eta')
        elif pt == 'v':
            P_dual[0,:,1:-1] = roms_grd['lon_u'].values
            P_dual[1,:,1:-1] = roms_grd['lat_u'].values
            _fill_bnd(P_dual, 'xi')

        # Fill in corner values
        P_crn[0,:,:,:] = P_dual[:, :-1, :-1]
        P_crn[1,:,:,:] = P_dual[:, :-1,1:  ]
        P_crn[2,:,:,:] = P_dual[:,1:  ,1:  ]
        P_crn[3,:,:,:] = P_dual[:,1:  , :-1]

        # Add to dataset
        PT = pt.upper()
        dims = ('crn',) + lon_pt.dims
        lon_name = 'lon_{:s}_crn'.format(pt)
        lat_name = 'lat_{:s}_crn'.format(pt)
        lon_long_name = 'longitude of {:s}-cells corners clockwise from south-east'.format(PT)
        lat_long_name = 'latitude of {:s}-cells corners clockwise from south-east'.format(PT)
        roms_grd[lon_name] = xr.DataArray(P_crn[:,0,:,:],
                                          dims=dims,
                                          attrs={'long_name': lon_long_name,
                                                 'units': 'degree_east'})
        roms_grd[lat_name] = xr.DataArray(P_crn[:,1,:,:],
                                          dims=dims,
                                          attrs={'long_name': lat_long_name,
                                                 'units': 'degree_north'})

    if return_ds:
        return roms_grd

from setuptools import setup

def get_version():
    with open('ROMSOC_tools/__init__.py') as f:
        for line in f:
            if line.startswith('__version__'):
                _, _, version = line.replace("'", '').split()
                break
    return version

setup(name='ROMSOC_tools',
      version=get_version(),
      description="Tools dedicated to the ROMSOC coupled model.",
      author="Matthieu Leclair",
      author_email="matthieu.leclair@env.ethz.ch",
      packages=['ROMSOC_tools'],
      entry_points={'console_scripts': ['romsoc = ROMSOC_tools.command_line:main']},
      install_requires=['numpy', 'netCDF4', 'xarray']
)
